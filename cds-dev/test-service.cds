using sf as _Test from '../db/test-data-model';

service TestService @(impl: './test-service.js', path: 'Test', requires: 'authenticated-user') {

	entity Test @(
		Capabilities: {
			InsertRestrictions: {Insertable: true},
			UpdateRestrictions: {Updatable:  true},
			DeleteRestrictions: {Deletable:  true}
		},
		restrict: [ { grant: ['*'], to: 'admin' }, ]
	)
	as projection on _Test.Test
	actions{
		action testAction() returns Test;
	};
}
