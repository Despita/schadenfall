namespace sf;

using {cuid} from '@sap/cds/common';

@cds.persistence.exists
  entity TableColumn {
    key tableName  : String;
    key columnName : String;
        position   : Integer64;
  };

entity Test : cuid {
    TestString  : String(100);
    TestBoolean : Boolean;
    TestDate    : Date;
    TestDecimal : Decimal(34, 6);
}
