namespace fsdp.ds;
using { sf.FinancialContract as _FinancialContract } from './FinancialContract-data-model';
using { sf.BusinessPartner as _BusinessPartner } from './BusinessPartner-data-model';
/* Definition of first level entity "Business Partner Contract Assignment" which is generated as "BusinessPartnerContractAssignment" on the DB.
		This table has been created by SAP and is made available here as Synonym
*/
@cds.persistence.exists
entity BusinessPartnerContractAssignment  {
	key Role: String(50) @( title: '{i18n>BusinessPartnerContractAssignment.Role}' );
	key BusinessValidFrom: Date @( title: '{i18n>BusinessPartnerContractAssignment.BusinessValidFrom}' );
	key BusinessValidTo: Date @( title: '{i18n>BusinessPartnerContractAssignment.BusinessValidTo}' );
	key ASSOC_PartnerInParticipation_BusinessPartnerID: String(128);
	key ASSOC_FinancialContract_FinancialContractID: String;
	key ASSOC_FinancialContract_IDSystem: String(40) @(UI.HiddenFilter: false);
	SystemValidFrom: Timestamp @( title: '{i18n>BusinessPartnerContractAssignment.SystemValidFrom}', UI.HiddenFilter: true  );
	SystemValidTo: Timestamp @( title: '{i18n>BusinessPartnerContractAssignment.SystemValidTo}', UI.HiddenFilter: true  );
	AutomobileAssociationMember: Boolean @( title: '{i18n>BusinessPartnerContractAssignment.AutomobileAssociationMember}', UI.HiddenFilter: true  );
	BusinessPartnerContractAssignmentCategory: String(40) @( title: '{i18n>BusinessPartnerContractAssignment.BusinessPartnerContractAssignmentCategory}', UI.HiddenFilter: true  );
	BusinessPartnerMakingTheOffer: Boolean @( title: '{i18n>BusinessPartnerContractAssignment.BusinessPartnerMakingTheOffer}', UI.HiddenFilter: true  );
	ContractDataOwner: Boolean @( title: '{i18n>BusinessPartnerContractAssignment.ContractDataOwner}', UI.HiddenFilter: true  );
	MainCounterparty: Boolean @( title: '{i18n>BusinessPartnerContractAssignment.MainCounterparty}', UI.HiddenFilter: true  );
	MainIndicator: Boolean @( title: '{i18n>BusinessPartnerContractAssignment.MainIndicator}', UI.HiddenFilter: true  );
	MarginPeriod: Integer @( title: '{i18n>BusinessPartnerContractAssignment.MarginPeriod}', UI.HiddenFilter: true  );
	MarginPeriodTimeUnit: String(10) @( title: '{i18n>BusinessPartnerContractAssignment.MarginPeriodTimeUnit}', UI.HiddenFilter: true  );
	MinimumTransferAmount: Decimal(34,6) @( title: '{i18n>BusinessPartnerContractAssignment.MinimumTransferAmount}', UI.HiddenFilter: true  );
	MinimumTransferAmountCurrency: String(3) @( title: '{i18n>BusinessPartnerContractAssignment.MinimumTransferAmountCurrency}', UI.HiddenFilter: true  );
	OwnerOccupiedPropertyType: String(40) @( title: '{i18n>BusinessPartnerContractAssignment.OwnerOccupiedPropertyType}', UI.HiddenFilter: true  );
	PowerOfAttorneyIndicator: Boolean @( title: '{i18n>BusinessPartnerContractAssignment.PowerOfAttorneyIndicator}', UI.HiddenFilter: true  );
	ReasonForPartnerAssignment: String(200) @( title: '{i18n>BusinessPartnerContractAssignment.ReasonForPartnerAssignment}', UI.HiddenFilter: true  );
	RoundingAmount: Decimal(34,6) @( title: '{i18n>BusinessPartnerContractAssignment.RoundingAmount}', UI.HiddenFilter: true  );
	RoundingAmountCurrency: String(3) @( title: '{i18n>BusinessPartnerContractAssignment.RoundingAmountCurrency}', UI.HiddenFilter: true  );
	TariffGroup: String(40) @( title: '{i18n>BusinessPartnerContractAssignment.TariffGroup}', UI.HiddenFilter: true  );
	ThresholdAmount: Decimal(34,6) @( title: '{i18n>BusinessPartnerContractAssignment.ThresholdAmount}', UI.HiddenFilter: true  );
	ThresholdAmountCurrency: String(3) @( title: '{i18n>BusinessPartnerContractAssignment.ThresholdAmountCurrency}', UI.HiddenFilter: true  );
	SourceSystemID: String(128) @( title: '{i18n>BusinessPartnerContractAssignment.SourceSystemID}', UI.HiddenFilter: true  );
	ChangeTimestampInSourceSystem: Timestamp @( title: '{i18n>BusinessPartnerContractAssignment.ChangeTimestampInSourceSystem}', UI.HiddenFilter: true  );
	ChangingUserInSourceSystem: String(128) @( title: '{i18n>BusinessPartnerContractAssignment.ChangingUserInSourceSystem}', UI.HiddenFilter: true  );
	ChangingProcessType: String(40) @( title: '{i18n>BusinessPartnerContractAssignment.ChangingProcessType}', UI.HiddenFilter: true  );
	ChangingProcessID: String(128) @( title: '{i18n>BusinessPartnerContractAssignment.ChangingProcessID}', UI.HiddenFilter: true  );
	ASSOC_FinancialContract: association to _FinancialContract on ASSOC_FinancialContract.FinancialContractID = ASSOC_FinancialContract_FinancialContractID and ASSOC_FinancialContract.IDSystem = ASSOC_FinancialContract_IDSystem @(UI.HiddenFilter: false);
	ASSOC_PartnerInParticipation: association to _BusinessPartner on ASSOC_PartnerInParticipation.BusinessPartnerID = ASSOC_PartnerInParticipation_BusinessPartnerID @(UI.HiddenFilter: false);
};
