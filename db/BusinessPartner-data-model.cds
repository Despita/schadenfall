namespace sf;

@cds.persistence.exists
@cds.odata.valuelist
entity BusinessPartner {
    key BusinessPartnerID                        : String(128)                                                                                       @(title : '{i18n>BusinessPartner.BusinessPartnerID}');
    key BusinessValidFrom                        : Date                                                                                              @(title : '{i18n>BusinessPartner.BusinessValidFrom}');
    key BusinessValidTo                          : Date                                                                                              @(title : '{i18n>BusinessPartner.BusinessValidTo}');
        SystemValidFrom                          : Timestamp                                                                                         @(title : '{i18n>BusinessPartner.SystemValidFrom}', );
        SystemValidTo                            : Timestamp                                                                                         @(title : '{i18n>BusinessPartner.SystemValidTo}', );
        AristocraticTitle                        : String(60)                                                                                        @(title : '{i18n>BusinessPartner.AristocraticTitle}', );
        BusinessPartnerCategory                  : String(40)                                                                                        @(title : '{i18n>BusinessPartner.BusinessPartnerCategory}', );
        ChildrenAtHomeIndicator                  : Boolean                                                                                           @(title : '{i18n>BusinessPartner.ChildrenAtHomeIndicator}', );
        ConcentrationFactor                      : Decimal(15, 11)                                                                                   @(title : '{i18n>BusinessPartner.ConcentrationFactor}', );
        Consultation                             : String(50)                                                                                        @(title : '{i18n>BusinessPartner.Consultation}', );
        ContributionsFromAllClearingMembers      : Decimal(34, 6)                                                                                    @(title : '{i18n>BusinessPartner.ContributionsFromAllClearingMembers}', );
        CountryOfBirth                           : String(2)                                                                                         @(title : '{i18n>BusinessPartner.CountryOfBirth}', );
        CountryOfFoundation                      : String(2)                                                                                         @(title : '{i18n>BusinessPartner.CountryOfFoundation}', );
        CountryOfIncorporation                   : String(2)                                                                                         @(title : '{i18n>BusinessPartner.CountryOfIncorporation}', );
        CountryOfMainEconomicActivities          : String(2)                                                                                         @(title : '{i18n>BusinessPartner.CountryOfMainEconomicActivities}', );
        DateOfBirth                              : Date                                                                                              @(title : '{i18n>BusinessPartner.DateOfBirth}', );
        DateOfDeath                              : Date                                                                                              @(title : '{i18n>BusinessPartner.DateOfDeath}', );
        DateOfFoundation                         : Date                                                                                              @(title : '{i18n>BusinessPartner.DateOfFoundation}', );
        DateOfIncorporation                      : Date                                                                                              @(title : '{i18n>BusinessPartner.DateOfIncorporation}', );
        DeceasedFlag                             : Boolean                                                                                           @(title : '{i18n>BusinessPartner.DeceasedFlag}', );
        DefaultFundCurrency                      : String(3)                                                                                         @(title : '{i18n>BusinessPartner.DefaultFundCurrency}', );
        EducationLevel                           : String(40)                                                                                        @(title : '{i18n>BusinessPartner.EducationLevel}', );
        EmployeeID                               : String(128)                                                                                       @(title : '{i18n>BusinessPartner.EmployeeID}', );
        EmploymentDate                           : Date                                                                                              @(title : '{i18n>BusinessPartner.EmploymentDate}', );
        FundType                                 : String(40)                                                                                        @(title : '{i18n>BusinessPartner.FundType}', );
        Gender                                   : String(40)                                                                                        @(title : '{i18n>BusinessPartner.Gender}', );
        GivenName                                : String(100)                                                                                       @(title : '{i18n>BusinessPartner.GivenName}', );
        GlobalSystemRelevantFinancialInstitution : Boolean                                                                                           @(title : '{i18n>BusinessPartner.GlobalSystemRelevantFinancialInstitution}', );
        GroupCategory                            : String(40)                                                                                        @(title : '{i18n>BusinessPartner.GroupCategory}', );
        GroupType                                : String(256)                                                                                       @(title : '{i18n>BusinessPartner.GroupType}', );
        HouseHoldGrossIncome                     : Decimal(34, 6)                                                                                    @(title : '{i18n>BusinessPartner.HouseHoldGrossIncome}', );
        HouseHoldGrossIncomeCurrency             : String(3)                                                                                         @(title : '{i18n>BusinessPartner.HouseHoldGrossIncomeCurrency}', );
        HouseHoldGrossIncomeTimeUnit             : String(12)                                                                                        @(title : '{i18n>BusinessPartner.HouseHoldGrossIncomeTimeUnit}', );
        HouseHoldNetIncome                       : Decimal(34, 6)                                                                                    @(title : '{i18n>BusinessPartner.HouseHoldNetIncome}', );
        HouseHoldNetIncomeCurrency               : String(3)                                                                                         @(title : '{i18n>BusinessPartner.HouseHoldNetIncomeCurrency}', );
        HouseHoldNetIncomeTimeUnit               : String(12)                                                                                        @(title : '{i18n>BusinessPartner.HouseHoldNetIncomeTimeUnit}', );
        HypotheticalCapitalRequirementsOfCCP     : Decimal(34, 6)                                                                                    @(title : '{i18n>BusinessPartner.HypotheticalCapitalRequirementsOfCCP}', );
        IndividualCategory                       : String(40)                                                                                        @(title : '{i18n>BusinessPartner.IndividualCategory}', );
        InstitutionalProtectionScheme            : String(256)                                                                                       @(title : '{i18n>BusinessPartner.InstitutionalProtectionScheme}', );
        IsCompany                                : Boolean                                                                                           @(title : '{i18n>BusinessPartner.IsCompany}', );
        IsHomeOwner                              : Boolean                                                                                           @(title : '{i18n>BusinessPartner.IsHomeOwner}', );
        IsPoliticallyExposedPerson               : Boolean                                                                                           @(title : '{i18n>BusinessPartner.IsPoliticallyExposedPerson}', );
        JobTitle                                 : String(60)                                                                                        @(title : '{i18n>BusinessPartner.JobTitle}', );
        JointLiabilityType                       : String(40)                                                                                        @(title : '{i18n>BusinessPartner.JointLiabilityType}', );
        LastName                                 : String(100)                                                                                       @(title : '{i18n>BusinessPartner.LastName}', );
        LeavingDate                              : Date                                                                                              @(title : '{i18n>BusinessPartner.LeavingDate}', );
        LegalEntityUnderPublicLawType            : String(256)                                                                                       @(title : '{i18n>BusinessPartner.LegalEntityUnderPublicLawType}', );
        LegalForm                                : String(50)                                                                                        @(title : '{i18n>BusinessPartner.LegalForm}', );
        LifecycleStatus                          : String(40)                                                                                        @(title : '{i18n>BusinessPartner.LifecycleStatus}', );
        LifecycleStatusChangeDate                : Date                                                                                              @(title : '{i18n>BusinessPartner.LifecycleStatusChangeDate}', );
        LifecycleStatusReason                    : String(256)                                                                                       @(title : '{i18n>BusinessPartner.LifecycleStatusReason}', );
        LiquidationDate                          : Date                                                                                              @(title : '{i18n>BusinessPartner.LiquidationDate}', );
        MaritalStatus                            : String(40)                                                                                        @(title : '{i18n>BusinessPartner.MaritalStatus}', );
        MiddleName                               : String(60)                                                                                        @(title : '{i18n>BusinessPartner.MiddleName}', );
        MinorToMajorDate                         : Date                                                                                              @(title : '{i18n>BusinessPartner.MinorToMajorDate}', );
        Name                                     : String(256)                                                                                       @(title : '{i18n>BusinessPartner.Name}', );
        NameAtBirth                              : String(256)                                                                                       @(title : '{i18n>BusinessPartner.NameAtBirth}', );
        NameSupplement                           : String(40)                                                                                        @(title : '{i18n>BusinessPartner.NameSupplement}', );
        Nationality                              : String(2)                                                                                         @(title : '{i18n>BusinessPartner.Nationality}', );
        NumberHouseholdMembers                   : Integer                                                                                           @(title : '{i18n>BusinessPartner.NumberHouseholdMembers}', );
        NumberOfClearingMembers                  : Integer                                                                                           @(title : '{i18n>BusinessPartner.NumberOfClearingMembers}', );
        OrganizationCategory                     : String(40)                                                                                        @(title : '{i18n>BusinessPartner.OrganizationCategory}', );
        PlaceOfBirth                             : String(60)                                                                                        @(title : '{i18n>BusinessPartner.PlaceOfBirth}', );
        PlaceOfRegister                          : String(256)                                                                                       @(title : '{i18n>BusinessPartner.PlaceOfRegister}', );
        PreferredCorrespondenceLanguage          : String(2)                                                                                         @(title : '{i18n>BusinessPartner.PreferredCorrespondenceLanguage}', );
        PrefundedOwnResourcesOfCCP               : Decimal(34, 6)                                                                                    @(title : '{i18n>BusinessPartner.PrefundedOwnResourcesOfCCP}', );
        QualifiedCCP                             : Boolean                                                                                           @(title : '{i18n>BusinessPartner.QualifiedCCP}', );
        Register                                 : String(256)                                                                                       @(title : '{i18n>BusinessPartner.Register}', );
        RegulatedFinancialInstitution            : Boolean                                                                                           @(title : '{i18n>BusinessPartner.RegulatedFinancialInstitution}', );
        RelationshipStartDate                    : Date                                                                                              @(title : '{i18n>BusinessPartner.RelationshipStartDate}', );
        ReligiousHonorific                       : String(60)                                                                                        @(title : '{i18n>BusinessPartner.ReligiousHonorific}', );
        ResidingCountry                          : String(2)                                                                                         @(title : '{i18n>BusinessPartner.ResidingCountry}', );
        ResidingSince                            : Date                                                                                              @(title : '{i18n>BusinessPartner.ResidingSince}', );
        ResidingStatus                           : String(100)                                                                                       @(title : '{i18n>BusinessPartner.ResidingStatus}', );
        RiskCountry                              : String(2)                                                                                         @(title : '{i18n>BusinessPartner.RiskCountry}', );
        RoleInClearing                           : String(50)                                                                                        @(title : '{i18n>BusinessPartner.RoleInClearing}', );
        SecondNationality                        : String(2)                                                                                         @(title : '{i18n>BusinessPartner.SecondNationality}', );
        ShortName                                : String(60)                                                                                        @(title : '{i18n>BusinessPartner.ShortName}', );
        SmokerIndicator                          : Boolean                                                                                           @(title : '{i18n>BusinessPartner.SmokerIndicator}', );
        Title                                    : String(40)                                                                                        @(title : '{i18n>BusinessPartner.Title}', );
        Type                                     : String(40)                                                                                        @(title : '{i18n>BusinessPartner.Type}', );
        SourceSystemID                           : String(128)                                                                                       @(title : '{i18n>BusinessPartner.SourceSystemID}', );
        ChangeTimestampInSourceSystem            : Timestamp                                                                                         @(title : '{i18n>BusinessPartner.ChangeTimestampInSourceSystem}', );
        ChangingUserInSourceSystem               : String(128)                                                                                       @(title : '{i18n>BusinessPartner.ChangingUserInSourceSystem}', );
        ChangingProcessType                      : String(40)                                                                                        @(title : '{i18n>BusinessPartner.ChangingProcessType}', );
        ChangingProcessID                        : String(128)                                                                                       @(title : '{i18n>BusinessPartner.ChangingProcessID}', );
        // BusinessPartnerClassification
        BusinessPartnerClassification            : Association to BusinessPartnerClassification
                                                       on  BusinessPartnerClassification.ASSOC_BusinessPartnerID_BusinessPartnerID = BusinessPartnerID
                                                       and BusinessPartnerClassification.BusinessValidFrom                         = BusinessValidFrom
                                                       and BusinessPartnerClassification.BusinessValidTo                           = BusinessValidTo @(title : '{i18n>BusinessPartner.BusinessPartnerClassification}');

};

@cds.persistence.exists
entity BusinessPartnerClassification {
    key ASSOC_BusinessPartnerID_BusinessPartnerID                         : String(128)@(title : '{i18n>BusinessPartner.BusinessPartnerID}');
    key ASSOC_ClassificationOfBusinessPartner_PartnerClass                : String(40) @(title : '{i18n>BusinessPartnerClassification.PartnerClass}');
    key ASSOC_ClassificationOfBusinessPartner_PartnerClassificationSystem : String(128)@(title : '{i18n>BusinessPartnerClassification.PartnerClassificationSystem}');
    key BusinessValidFrom                                                 : Date       @(title : '{i18n>BusinessPartner.BusinessValidFrom}');
    key BusinessValidTo                                                   : Date       @(title : '{i18n>BusinessPartner.BusinessValidTo}');
        SystemValidFrom                                                   : Timestamp  @(title : '{i18n>BusinessPartner.SystemValidFrom}');
        SystemValidTo                                                     : Timestamp  @(title : '{i18n>BusinessPartner.SystemValidTo}');
        SourceSystemID                                                    : String(128)@(title : '{i18n>BusinessPartnerClassification.SourceSystemID}');
        ChangeTimestampInSourceSystem                                     : String(128)@(title : '{i18n>BusinessPartnerClassification.ChangeTimestampInSourceSystem}');
        ChangingUserInSourceSystem                                        : String(128)@(title : '{i18n>BusinessPartnerClassification.ChangingUserInSourceSystem}');
        ChangingProcessType                                               : String(128)@(title : '{i18n>BusinessPartnerClassification.ChangingProcessType}');
        ChangingProcessID                                                 : String(128)@(title : '{i18n>BusinessPartnerClassification.ChangingProcessID}');
}
