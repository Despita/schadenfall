namespace sf;

using {
    cuid,
    sap.common.CodeList
} from '@sap/cds/common';
using {sf.BusinessPartnerClassification as _BusinessPartnerClassification} from './BusinessPartner-data-model';

@Aggregation.ApplySupported.PropertyRestrictions : true
entity Claim : cuid {
    BusinessPartnerID             : String(128)                                                                                       @(title : '{i18n>BusinessPartner.BusinessPartnerID}');
    LoanID                        : String(128)                                                                                       @(title : '{i18n>Claim.LoanID}');
    IDSystem                      : String(40)                                                                                        @(title : '{i18n>FinancialContract.IDSystem}');
    BusinessValidFrom             : Date                                                                                              @(title : '{i18n>Claim.BusinessValidFrom}');
    BusinessValidTo               : Date                                                                                              @(title : '{i18n>Claim.BusinessValidTo}');
    @Analytics.Measure
    @Aggregation.default : #SUM
    Amount                        : Decimal(34, 6)                                                                                    @(title : '{i18n>Claim.Amount}');
    LastRatingDate                : Date                                                                                              @(title : '{i18n>Claim.LastRatingDate}');
    Status                        : Association to ClaimStatus                                                                        @(title : '{i18n>Claim.ClaimStatus}')  @assert.integrity : false;
    Processed                     : Boolean                                                                                           @(title : '{i18n>Claim.Processed}');
    Currency                      : String(3)                                                                                         @(title : '{i18n>Claim.Currency}');
    Rating                        : String(10)                                                                                        @(title : '{i18n>Rating.Rating}');
    BusinessPartnerClassification : Association to _BusinessPartnerClassification
                                        on  BusinessPartnerClassification.ASSOC_BusinessPartnerID_BusinessPartnerID = BusinessPartnerID
                                        and BusinessPartnerClassification.BusinessValidFrom                         >= BusinessValidFrom
                                        and BusinessPartnerClassification.BusinessValidTo                           <= BusinessValidTo @(title : '{i18n>Claim.BusinessPartnerClassification)');
}
entity ClaimStatus : CodeList {
    key code : String enum {
            Open                 = 'O';
            ApplicationRequested = 'A';
        };
}
