namespace sf;

@cds.persistence.exists
entity Rating {
    key IsFxRating                                           : Boolean        @(title : '{i18n>Rating.IsFxRating}');
    key RatingAgency                                         : String(128)    @(title : '{i18n>Rating.RatingAgency}');
    key RatingCategory                                       : String(40)     @(title : '{i18n>Rating.RatingCategory}');
    key RatingMethod                                         : String(60)     @(title : '{i18n>Rating.RatingMethod}');
    key RatingStatus                                         : String(40)     @(title : '{i18n>Rating.RatingStatus}');
    key TimeHorizon                                          : String(10)     @(title : '{i18n>Rating.TimeHorizon}');
    key ASSOC_BusinessPartner_BusinessPartnerID              : String(128)    @(title : '{i18n>BusinessPartner.BusinessPartnerID}');
    key ASSOC_FinancialContract_FinancialContractID          : String(128)    @(title : '{i18n>FinancialContract.FinancialContractID}');
    key ASSOC_FinancialContract_IDSystem                     : String(40)     @(title : '{i18n>FinancialContract.IDSystem}');
    key ASSOC_GeographicalRegion_GeographicalStructureID     : String(128)    @(title : '{i18n>Rating.GeographicalStructureID}');
    key ASSOC_GeographicalRegion_GeographicalUnitID          : String(10)     @(title : '{i18n>Rating.GeographicalUnitID}');
    key _Security_FinancialInstrumentID                      : String(128)    @(title : '{i18n>Rating.FinancialInstrumentID}');
    key BusinessValidFrom                                    : Date           @(title : '{i18n>BusinessPartner.BusinessValidFrom}');
    key BusinessValidTo                                      : Date           @(title : '{i18n>BusinessPartner.BusinessValidTo}');
        SystemValidFrom                                      : Timestamp      @(title : '{i18n>BusinessPartner.SystemValidFrom}');
        SystemValidTo                                        : Timestamp      @(title : '{i18n>BusinessPartner.SystemValidTo}');
        ASSOC_EmployeeResponsibleForRating_BusinessPartnerID : String(128)    @(title : '{i18n>BusinessPartner.BusinessPartnerID}');
        _EmployeeApprovingTheRating_BusinessPartnerID        : String(128)    @(title : '{i18n>BusinessPartner.BusinessPartnerID}');
        ApprovalDate                                         : Date           @(title : '{i18n>Rating.ApprovalDate}');
        Commissioned                                         : Boolean        @(title : '{i18n>Rating.Commissioned}');
        CreationDate                                         : Date           @(title : '{i18n>Rating.CreationDate}');
        Endorsed                                             : Boolean        @(title : '{i18n>Rating.Endorsed}');
        MarginOfConservatismCategoryA                        : Decimal(15, 11)@(title : '{i18n>Rating.MarginOfConservatismCategoryA}');
        MarginOfConservatismCategoryB                        : Decimal(15, 11)@(title : '{i18n>Rating.MarginOfConservatismCategoryB}');
        MarginOfConservatismCategoryC                        : Decimal(15, 11)@(title : '{i18n>Rating.MarginOfConservatismCategoryC}');
        PublicationDate                                      : Date           @(title : '{i18n>Rating.PublicationDate}');
        Rating                                               : String(10)     @(title : '{i18n>Rating.Rating}');
        RatingBasedOnMarginsOfConservatism                   : String(10)     @(title : '{i18n>Rating.RatingBasedOnMarginsOfConservatism}');
        RatingComment                                        : String(200)    @(title : '{i18n>Rating.RatingComment}');
        RatingDescription                                    : String(256)    @(title : '{i18n>Rating.RatingDescription}');
        RatingMethodVersion                                  : String(100)    @(title : '{i18n>Rating.RatingMethodVersion}');
        RatingOutlook                                        : String(40)     @(title : '{i18n>Rating.RatingOutlook}');
        RatingRank                                           : Integer        @(title : '{i18n>Rating.RatingRank}');
        RatingStatusChangeDate                               : Date           @(title : '{i18n>Rating.RatingStatusChangeDate}');
        RatingStatusReason                                   : String(256)    @(title : '{i18n>Rating.RatingStatusReason}');
        SourceSystemID                                       : String(128)    @(title : '{i18n>Rating.SourceSystemID}');
        ChangeTimestampInSourceSystem                        : Timestamp      @(title : '{i18n>Rating.ChangeTimestampInSourceSystem}');
        ChangingUserInSourceSystem                           : String(128)    @(title : '{i18n>Rating.ChangingUserInSourceSystem}');
        ChangingProcessType                                  : String(40)     @(title : '{i18n>Rating.ChangingProcessType}');
        ChangingProcessID                                    : String(128)    @(title : '{i18n>Rating.ChangingProcessID}');
}
