namespace sf;
using { sf.FinancialContract as _FinancialContract } from './FinancialContract-data-model';
using { sf.PhysicalAsset as _PhysicalAsset } from './PhysicalAsset-data-model';

@cds.persistence.exists
entity FinancialContract {
	key FinancialContractID: String(128) @( title: '{i18n>FinancialContract.FinancialContractID}' );
	key IDSystem: String(40) @( title: '{i18n>FinancialContract.IDSystem}' );
	key BusinessValidFrom: Date @( title: '{i18n>FinancialContract.BusinessValidFrom}' );
	key BusinessValidTo: Date @( title: '{i18n>FinancialContract.BusinessValidTo}' );
	SystemValidFrom: Timestamp @( title: '{i18n>FinancialContract.SystemValidFrom}', UI.HiddenFilter: true  );
	SystemValidTo: Timestamp @( title: '{i18n>FinancialContract.SystemValidTo}', UI.HiddenFilter: true  );
	ABSStructureType: String(60) @( title: '{i18n>FinancialContract.ABSStructureType}', UI.HiddenFilter: true  );
	AcceptanceDate: Date @( title: '{i18n>FinancialContract.AcceptanceDate}', UI.HiddenFilter: true  );
	AccountCategory: String(40) @( title: '{i18n>FinancialContract.AccountCategory}', UI.HiddenFilter: true  );
	AccountCurrency: String(3) @( title: '{i18n>FinancialContract.AccountCurrency}', UI.HiddenFilter: true  );
	AccountIdentificationSystem: String(128) @( title: '{i18n>FinancialContract.AccountIdentificationSystem}', UI.HiddenFilter: true  );
	AccountValueWithdrawalRule: String(40) @( title: '{i18n>FinancialContract.AccountValueWithdrawalRule}', UI.HiddenFilter: true  );
	AccountingDataBasisIndicator: Boolean @( title: '{i18n>FinancialContract.AccountingDataBasisIndicator}', UI.HiddenFilter: true  );
	AccrualAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.AccrualAmount}', UI.HiddenFilter: true  );
	AccrualAmountCurrency: String(3) @( title: '{i18n>FinancialContract.AccrualAmountCurrency}', UI.HiddenFilter: true  );
	AcquisitionYear: Integer @( title: '{i18n>FinancialContract.AcquisitionYear}', UI.HiddenFilter: true  );
	ActionAgainstBeneficiaryNotRequired: Boolean @( title: '{i18n>FinancialContract.ActionAgainstBeneficiaryNotRequired}', UI.HiddenFilter: true  );
	AgreedNewEndDateAfterProlongation: Date @( title: '{i18n>FinancialContract.AgreedNewEndDateAfterProlongation}', UI.HiddenFilter: true  );
	AgreementEndDate: Date @( title: '{i18n>FinancialContract.AgreementEndDate}', UI.HiddenFilter: true  );
	AgreementStartDate: Date @( title: '{i18n>FinancialContract.AgreementStartDate}', UI.HiddenFilter: true  );
	AllocationOptionFactor: Decimal(15,11) @( title: '{i18n>FinancialContract.AllocationOptionFactor}', UI.HiddenFilter: true  );
	AmortizationType: String(40) @( title: '{i18n>FinancialContract.AmortizationType}', UI.HiddenFilter: true  );
	AmountDueAtLeaseSigning: Decimal(34,6) @( title: '{i18n>FinancialContract.AmountDueAtLeaseSigning}', UI.HiddenFilter: true  );
	AmountDueAtLeaseSigningCurrency: String(3) @( title: '{i18n>FinancialContract.AmountDueAtLeaseSigningCurrency}', UI.HiddenFilter: true  );
	AmountInTradedCurrency: Decimal(34,6) @( title: '{i18n>FinancialContract.AmountInTradedCurrency}', UI.HiddenFilter: true  );
	ApplicableLaw: String(256) @( title: '{i18n>FinancialContract.ApplicableLaw}', UI.HiddenFilter: true  );
	ApplicableRules: String(50) @( title: '{i18n>FinancialContract.ApplicableRules}', UI.HiddenFilter: true  );
	ApprovalDate: Date @( title: '{i18n>FinancialContract.ApprovalDate}', UI.HiddenFilter: true  );
	ApprovedNominalAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.ApprovedNominalAmount}', UI.HiddenFilter: true  );
	AssignmentDisclosedToObligor: Boolean @( title: '{i18n>FinancialContract.AssignmentDisclosedToObligor}', UI.HiddenFilter: true  );
	AssignmentOfClaimsType: String(40) @( title: '{i18n>FinancialContract.AssignmentOfClaimsType}', UI.HiddenFilter: true  );
	AttachmentPoint: Decimal(34,5) @( title: '{i18n>FinancialContract.AttachmentPoint}', UI.HiddenFilter: true  );
	BankAccountCurrency: String(3) @( title: '{i18n>FinancialContract.BankAccountCurrency}', UI.HiddenFilter: true  );
	BankAccountHolderName: String(256) @( title: '{i18n>FinancialContract.BankAccountHolderName}', UI.HiddenFilter: true  );
	BankAccountID: String(128) @( title: '{i18n>FinancialContract.BankAccountID}', UI.HiddenFilter: true  );
	BankGuaranteeType: String(50) @( title: '{i18n>FinancialContract.BankGuaranteeType}', UI.HiddenFilter: true  );
	BankID: String(128) @( title: '{i18n>FinancialContract.BankID}', UI.HiddenFilter: true  );
	BankIdentificationSystem: String(128) @( title: '{i18n>FinancialContract.BankIdentificationSystem}', UI.HiddenFilter: true  );
	BankName: String(256) @( title: '{i18n>FinancialContract.BankName}', UI.HiddenFilter: true  );
	BenchmarkID: String(256) @( title: '{i18n>FinancialContract.BenchmarkID}', UI.HiddenFilter: true  );
	Binary: Boolean @( title: '{i18n>FinancialContract.Binary}', UI.HiddenFilter: true  );
	BinaryPayOffAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.BinaryPayOffAmount}', UI.HiddenFilter: true  );
	BinaryPayOffAmountCurrency: String(3) @( title: '{i18n>FinancialContract.BinaryPayOffAmountCurrency}', UI.HiddenFilter: true  );
	BondNotional: Decimal(34,6) @( title: '{i18n>FinancialContract.BondNotional}', UI.HiddenFilter: true  );
	BondNotionalCurrency: String(3) @( title: '{i18n>FinancialContract.BondNotionalCurrency}', UI.HiddenFilter: true  );
	BoundIndicator: Boolean @( title: '{i18n>FinancialContract.BoundIndicator}', UI.HiddenFilter: true  );
	BrokerIsLiable: Boolean @( title: '{i18n>FinancialContract.BrokerIsLiable}', UI.HiddenFilter: true  );
	BusinessCalendar: String(50) @( title: '{i18n>FinancialContract.BusinessCalendar}', UI.HiddenFilter: true  );
	BusinessClass: String(40) @( title: '{i18n>FinancialContract.BusinessClass}', UI.HiddenFilter: true  );
	BusinessClassCode: String(40) @( title: '{i18n>FinancialContract.BusinessClassCode}', UI.HiddenFilter: true  );
	BusinessDayConvention: String(40) @( title: '{i18n>FinancialContract.BusinessDayConvention}', UI.HiddenFilter: true  );
	BusinessDirectionCode: String(40) @( title: '{i18n>FinancialContract.BusinessDirectionCode}', UI.HiddenFilter: true  );
	BuyerAllowedToRetainCollateral: Boolean @( title: '{i18n>FinancialContract.BuyerAllowedToRetainCollateral}', UI.HiddenFilter: true  );
	BuyerCanChooseCashSettlement: Boolean @( title: '{i18n>FinancialContract.BuyerCanChooseCashSettlement}', UI.HiddenFilter: true  );
	CancellationDate: Date @( title: '{i18n>FinancialContract.CancellationDate}', UI.HiddenFilter: true  );
	CancellationReason: String(40) @( title: '{i18n>FinancialContract.CancellationReason}', UI.HiddenFilter: true  );
	CancellationStatus: String(40) @( title: '{i18n>FinancialContract.CancellationStatus}', UI.HiddenFilter: true  );
	CapitalCompensationIndicator: Boolean @( title: '{i18n>FinancialContract.CapitalCompensationIndicator}', UI.HiddenFilter: true  );
	CardAccountManagedByThirdParty: Boolean @( title: '{i18n>FinancialContract.CardAccountManagedByThirdParty}', UI.HiddenFilter: true  );
	CardAgreementCategory: String(40) @( title: '{i18n>FinancialContract.CardAgreementCategory}', UI.HiddenFilter: true  );
	CashPoolingIndicator: Boolean @( title: '{i18n>FinancialContract.CashPoolingIndicator}', UI.HiddenFilter: true  );
	CashSettlement: Boolean @( title: '{i18n>FinancialContract.CashSettlement}', UI.HiddenFilter: true  );
	CashSettlementDate: Date @( title: '{i18n>FinancialContract.CashSettlementDate}', UI.HiddenFilter: true  );
	CashSettlementMethod: String(30) @( title: '{i18n>FinancialContract.CashSettlementMethod}', UI.HiddenFilter: true  );
	CashSettlementTime: Time @( title: '{i18n>FinancialContract.CashSettlementTime}', UI.HiddenFilter: true  );
	CashSettlementTimeZone: String(100) @( title: '{i18n>FinancialContract.CashSettlementTimeZone}', UI.HiddenFilter: true  );
	CedentAccountForRetentionIndicator: Boolean @( title: '{i18n>FinancialContract.CedentAccountForRetentionIndicator}', UI.HiddenFilter: true  );
	CedentFinancialReportingFrequencyCode: String(40) @( title: '{i18n>FinancialContract.CedentFinancialReportingFrequencyCode}', UI.HiddenFilter: true  );
	CedentOriginCountry: String(2) @( title: '{i18n>FinancialContract.CedentOriginCountry}', UI.HiddenFilter: true  );
	CedentPremiumPaymentFrequency: String(40) @( title: '{i18n>FinancialContract.CedentPremiumPaymentFrequency}', UI.HiddenFilter: true  );
	CharacteristicCategoryCode: String(40) @( title: '{i18n>FinancialContract.CharacteristicCategoryCode}', UI.HiddenFilter: true  );
	CharacteristicSubCategoryCode: String(40) @( title: '{i18n>FinancialContract.CharacteristicSubCategoryCode}', UI.HiddenFilter: true  );
	ClaimAdministrationClauseCode: String(40) @( title: '{i18n>FinancialContract.ClaimAdministrationClauseCode}', UI.HiddenFilter: true  );
	CleanPrice: Decimal(34,6) @( title: '{i18n>FinancialContract.CleanPrice}', UI.HiddenFilter: true  );
	CloseOutNettingIndicator: Boolean @( title: '{i18n>FinancialContract.CloseOutNettingIndicator}', UI.HiddenFilter: true  );
	ClosingDate: Date @( title: '{i18n>FinancialContract.ClosingDate}', UI.HiddenFilter: true  );
	CollateralAgreementCategory: String(40) @( title: '{i18n>FinancialContract.CollateralAgreementCategory}', UI.HiddenFilter: true  );
	CollateralRebateRate: Decimal(15,11) @( title: '{i18n>FinancialContract.CollateralRebateRate}', UI.HiddenFilter: true  );
	CollateralType: String(60) @( title: '{i18n>FinancialContract.CollateralType}', UI.HiddenFilter: true  );
	CollectionAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.CollectionAmount}', UI.HiddenFilter: true  );
	CollectionAmountCurrency: String(3) @( title: '{i18n>FinancialContract.CollectionAmountCurrency}', UI.HiddenFilter: true  );
	CollectionPlace: String(256) @( title: '{i18n>FinancialContract.CollectionPlace}', UI.HiddenFilter: true  );
	CommitmentAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.CommitmentAmount}', UI.HiddenFilter: true  );
	CommitmentAmountCurrency: String(3) @( title: '{i18n>FinancialContract.CommitmentAmountCurrency}', UI.HiddenFilter: true  );
	CommitmentEndDate: Date @( title: '{i18n>FinancialContract.CommitmentEndDate}', UI.HiddenFilter: true  );
	CommitmentStartDate: Date @( title: '{i18n>FinancialContract.CommitmentStartDate}', UI.HiddenFilter: true  );
	CommitmentType: String(40) @( title: '{i18n>FinancialContract.CommitmentType}', UI.HiddenFilter: true  );
	CommunicatedEffectiveInterestRate: Decimal(15,11) @( title: '{i18n>FinancialContract.CommunicatedEffectiveInterestRate}', UI.HiddenFilter: true  );
	CommunicatedEffectiveInterestRateMethod: String(20) @( title: '{i18n>FinancialContract.CommunicatedEffectiveInterestRateMethod}', UI.HiddenFilter: true  );
	CommutationProcessingType: String(40) @( title: '{i18n>FinancialContract.CommutationProcessingType}', UI.HiddenFilter: true  );
	ConfirmedAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.ConfirmedAmount}', UI.HiddenFilter: true  );
	ConfirmedAmountCurrency: String(3) @( title: '{i18n>FinancialContract.ConfirmedAmountCurrency}', UI.HiddenFilter: true  );
	ContractAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.ContractAmount}', UI.HiddenFilter: true  );
	ContractAmountCurrency: String(3) @( title: '{i18n>FinancialContract.ContractAmountCurrency}', UI.HiddenFilter: true  );
	ContractualCurrency: String(3) @( title: '{i18n>FinancialContract.ContractualCurrency}', UI.HiddenFilter: true  );
	CoverPoolEligibility: String(40) @( title: '{i18n>FinancialContract.CoverPoolEligibility}', UI.HiddenFilter: true  );
	CreditAvailableByMethod: String(50) @( title: '{i18n>FinancialContract.CreditAvailableByMethod}', UI.HiddenFilter: true  );
	CreditAvailableWithRole: String(50) @( title: '{i18n>FinancialContract.CreditAvailableWithRole}', UI.HiddenFilter: true  );
	CreditCardCategorybyCardAccount: String(100) @( title: '{i18n>FinancialContract.CreditCardCategorybyCardAccount}', UI.HiddenFilter: true  );
	CreditCardCategorybySettlementAccount: String(100) @( title: '{i18n>FinancialContract.CreditCardCategorybySettlementAccount}', UI.HiddenFilter: true  );
	CreditDefaultSwapCategory: String(40) @( title: '{i18n>FinancialContract.CreditDefaultSwapCategory}', UI.HiddenFilter: true  );
	CreditSubstitute: Boolean @( title: '{i18n>FinancialContract.CreditSubstitute}', UI.HiddenFilter: true  );
	CurePeriodLength: Decimal(34,6) @( title: '{i18n>FinancialContract.CurePeriodLength}', UI.HiddenFilter: true  );
	CurePeriodTimeUnit: String(12) @( title: '{i18n>FinancialContract.CurePeriodTimeUnit}', UI.HiddenFilter: true  );
	CurrentDepositEndDate: Date @( title: '{i18n>FinancialContract.CurrentDepositEndDate}', UI.HiddenFilter: true  );
	CustomerFacingBankAccountID: String(128) @( title: '{i18n>FinancialContract.CustomerFacingBankAccountID}', UI.HiddenFilter: true  );
	CustomerFacingBankAccountIdentificationSystem: String(128) @( title: '{i18n>FinancialContract.CustomerFacingBankAccountIdentificationSystem}', UI.HiddenFilter: true  );
	CustomerFacingBankID: String(128) @( title: '{i18n>FinancialContract.CustomerFacingBankID}', UI.HiddenFilter: true  );
	CustomerFacingBankIdentificationSystem: String(128) @( title: '{i18n>FinancialContract.CustomerFacingBankIdentificationSystem}', UI.HiddenFilter: true  );
	DayCountConvention: String(20) @( title: '{i18n>FinancialContract.DayCountConvention}', UI.HiddenFilter: true  );
	DeliveryDate: Date @( title: '{i18n>FinancialContract.DeliveryDate}', UI.HiddenFilter: true  );
	DenominationCurrency: String(3) @( title: '{i18n>FinancialContract.DenominationCurrency}', UI.HiddenFilter: true  );
	DenominationCurrencyOfInstrumentBorrowed: String(3) @( title: '{i18n>FinancialContract.DenominationCurrencyOfInstrumentBorrowed}', UI.HiddenFilter: true  );
	DenominationCurrencyOfInstrumentLent: String(3) @( title: '{i18n>FinancialContract.DenominationCurrencyOfInstrumentLent}', UI.HiddenFilter: true  );
	DepositStartDate: Date @( title: '{i18n>FinancialContract.DepositStartDate}', UI.HiddenFilter: true  );
	Description: String(256) @( title: '{i18n>FinancialContract.Description}', UI.HiddenFilter: true  );
	DescriptionOfRequiredDocuments: String(200) @( title: '{i18n>FinancialContract.DescriptionOfRequiredDocuments}', UI.HiddenFilter: true  );
	DirtyPrice: Decimal(34,6) @( title: '{i18n>FinancialContract.DirtyPrice}', UI.HiddenFilter: true  );
	Discount: Decimal(15,11) @( title: '{i18n>FinancialContract.Discount}', UI.HiddenFilter: true  );
	DiscountAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.DiscountAmount}', UI.HiddenFilter: true  );
	DiscountAmountCurrency: String(3) @( title: '{i18n>FinancialContract.DiscountAmountCurrency}', UI.HiddenFilter: true  );
	DiscountPeriodLength: Decimal(34,6) @( title: '{i18n>FinancialContract.DiscountPeriodLength}', UI.HiddenFilter: true  );
	DiscountPeriodTimeUnit: String(12) @( title: '{i18n>FinancialContract.DiscountPeriodTimeUnit}', UI.HiddenFilter: true  );
	DiscountingMethod: String(20) @( title: '{i18n>FinancialContract.DiscountingMethod}', UI.HiddenFilter: true  );
	DistributionChannel: String(40) @( title: '{i18n>FinancialContract.DistributionChannel}', UI.HiddenFilter: true  );
	DocumentaryCollectionType: String(50) @( title: '{i18n>FinancialContract.DocumentaryCollectionType}', UI.HiddenFilter: true  );
	DueDate: Date @( title: '{i18n>FinancialContract.DueDate}', UI.HiddenFilter: true  );
	DueDayMonth: String(5) @( title: '{i18n>FinancialContract.DueDayMonth}', UI.HiddenFilter: true  );
	EffectiveDate: Date @( title: '{i18n>FinancialContract.EffectiveDate}', UI.HiddenFilter: true  );
	EndDate: Date @( title: '{i18n>FinancialContract.EndDate}', UI.HiddenFilter: true  );
	EndOfCedentAccountingPeriodDayMonth: String(5) @( title: '{i18n>FinancialContract.EndOfCedentAccountingPeriodDayMonth}', UI.HiddenFilter: true  );
	Enforceable: Boolean @( title: '{i18n>FinancialContract.Enforceable}', UI.HiddenFilter: true  );
	EquityInvestmentEndDate: Date @( title: '{i18n>FinancialContract.EquityInvestmentEndDate}', UI.HiddenFilter: true  );
	EquityInvestmentStartDate: Date @( title: '{i18n>FinancialContract.EquityInvestmentStartDate}', UI.HiddenFilter: true  );
	EquityStakePercentage: Decimal(15,11) @( title: '{i18n>FinancialContract.EquityStakePercentage}', UI.HiddenFilter: true  );
	EquityVotingRightsPercentage: Decimal(15,11) @( title: '{i18n>FinancialContract.EquityVotingRightsPercentage}', UI.HiddenFilter: true  );
	ExerciseMethod: String(30) @( title: '{i18n>FinancialContract.ExerciseMethod}', UI.HiddenFilter: true  );
	ExerciseStyle: String(10) @( title: '{i18n>FinancialContract.ExerciseStyle}', UI.HiddenFilter: true  );
	ExhaustionPoint: Decimal(34,5) @( title: '{i18n>FinancialContract.ExhaustionPoint}', UI.HiddenFilter: true  );
	ExpectedAllocationDate: Date @( title: '{i18n>FinancialContract.ExpectedAllocationDate}', UI.HiddenFilter: true  );
	ExpectedMaturityDate: Date @( title: '{i18n>FinancialContract.ExpectedMaturityDate}', UI.HiddenFilter: true  );
	ExpirationDate: Date @( title: '{i18n>FinancialContract.ExpirationDate}', UI.HiddenFilter: true  );
	ExpirationTime: Time @( title: '{i18n>FinancialContract.ExpirationTime}', UI.HiddenFilter: true  );
	ExpirationTimeZone: String(100) @( title: '{i18n>FinancialContract.ExpirationTimeZone}', UI.HiddenFilter: true  );
	ExpiryDate: Date @( title: '{i18n>FinancialContract.ExpiryDate}', UI.HiddenFilter: true  );
	ExpiryPlace: String(256) @( title: '{i18n>FinancialContract.ExpiryPlace}', UI.HiddenFilter: true  );
	FRAFixingDate: Date @( title: '{i18n>FinancialContract.FRAFixingDate}', UI.HiddenFilter: true  );
	FacilityEndDate: Date @( title: '{i18n>FinancialContract.FacilityEndDate}', UI.HiddenFilter: true  );
	FacilityPurpose: String(80) @( title: '{i18n>FinancialContract.FacilityPurpose}', UI.HiddenFilter: true  );
	FacilityStartDate: Date @( title: '{i18n>FinancialContract.FacilityStartDate}', UI.HiddenFilter: true  );
	FacilityType: String(40) @( title: '{i18n>FinancialContract.FacilityType}', UI.HiddenFilter: true  );
	FactoringAgreementType: String(100) @( title: '{i18n>FinancialContract.FactoringAgreementType}', UI.HiddenFilter: true  );
	FinancialContractCategory: String(50) @( title: '{i18n>FinancialContract.FinancialContractCategory}', UI.HiddenFilter: true  );
	FinancialContractType: String(40) @( title: '{i18n>FinancialContract.FinancialContractType}', UI.HiddenFilter: true  );
	FinancingAmountCurrency: String(3) @( title: '{i18n>FinancialContract.FinancingAmountCurrency}', UI.HiddenFilter: true  );
	FirstAccrualStartDate: Date @( title: '{i18n>FinancialContract.FirstAccrualStartDate}', UI.HiddenFilter: true  );
	FirstCouponPaymentDate: Date @( title: '{i18n>FinancialContract.FirstCouponPaymentDate}', UI.HiddenFilter: true  );
	FirstSettlementDate: Date @( title: '{i18n>FinancialContract.FirstSettlementDate}', UI.HiddenFilter: true  );
	FixedDepositRepayableAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.FixedDepositRepayableAmount}', UI.HiddenFilter: true  );
	FixedPeriodEndDate: Date @( title: '{i18n>FinancialContract.FixedPeriodEndDate}', UI.HiddenFilter: true  );
	FixedPeriodStartDate: Date @( title: '{i18n>FinancialContract.FixedPeriodStartDate}', UI.HiddenFilter: true  );
	FixedRate: Decimal(34,6) @( title: '{i18n>FinancialContract.FixedRate}', UI.HiddenFilter: true  );
	FixingLagLength: Decimal(34,6) @( title: '{i18n>FinancialContract.FixingLagLength}', UI.HiddenFilter: true  );
	FixingLagTimeUnit: String(12) @( title: '{i18n>FinancialContract.FixingLagTimeUnit}', UI.HiddenFilter: true  );
	ForClientTrades: Boolean @( title: '{i18n>FinancialContract.ForClientTrades}', UI.HiddenFilter: true  );
	FormofParticipation: String(40) @( title: '{i18n>FinancialContract.FormofParticipation}', UI.HiddenFilter: true  );
	ForwardPrice: Decimal(34,6) @( title: '{i18n>FinancialContract.ForwardPrice}', UI.HiddenFilter: true  );
	ForwardPriceAdjustment: Boolean @( title: '{i18n>FinancialContract.ForwardPriceAdjustment}', UI.HiddenFilter: true  );
	ForwardPriceCurrency: String(3) @( title: '{i18n>FinancialContract.ForwardPriceCurrency}', UI.HiddenFilter: true  );
	FrontingIndicator: Boolean @( title: '{i18n>FinancialContract.FrontingIndicator}', UI.HiddenFilter: true  );
	GoverningLawCountry: String(2) @( title: '{i18n>FinancialContract.GoverningLawCountry}', UI.HiddenFilter: true  );
	GracePeriod: Integer @( title: '{i18n>FinancialContract.GracePeriod}', UI.HiddenFilter: true  );
	GrossInvoiceAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.GrossInvoiceAmount}', UI.HiddenFilter: true  );
	GrossInvoiceAmountCurrency: String(3) @( title: '{i18n>FinancialContract.GrossInvoiceAmountCurrency}', UI.HiddenFilter: true  );
	GuaranteeCategory: String(40) @( title: '{i18n>FinancialContract.GuaranteeCategory}', UI.HiddenFilter: true  );
	GuaranteePercentage: Decimal(15,11) @( title: '{i18n>FinancialContract.GuaranteePercentage}', UI.HiddenFilter: true  );
	GuaranteeSettlementCurrency: String(3) @( title: '{i18n>FinancialContract.GuaranteeSettlementCurrency}', UI.HiddenFilter: true  );
	Haircut: Decimal(15,11) @( title: '{i18n>FinancialContract.Haircut}', UI.HiddenFilter: true  );
	HedgePurpose: String(30) @( title: '{i18n>FinancialContract.HedgePurpose}', UI.HiddenFilter: true  );
	HomeLoanAndSavingsTariff: String(256) @( title: '{i18n>FinancialContract.HomeLoanAndSavingsTariff}', UI.HiddenFilter: true  );
	IBAN: String(128) @( title: '{i18n>FinancialContract.IBAN}', UI.HiddenFilter: true  );
	ImplicitSupport: Boolean @( title: '{i18n>FinancialContract.ImplicitSupport}', UI.HiddenFilter: true  );
	IncomeTaxExemptionAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.IncomeTaxExemptionAmount}', UI.HiddenFilter: true  );
	IncomeTaxExemptionPercent: Decimal(15,11) @( title: '{i18n>FinancialContract.IncomeTaxExemptionPercent}', UI.HiddenFilter: true  );
	IncomeTaxExemptionType: String(40) @( title: '{i18n>FinancialContract.IncomeTaxExemptionType}', UI.HiddenFilter: true  );
	IndexMultiplier: Decimal(34,6) @( title: '{i18n>FinancialContract.IndexMultiplier}', UI.HiddenFilter: true  );
	IndexMultiplierCurrency: String(3) @( title: '{i18n>FinancialContract.IndexMultiplierCurrency}', UI.HiddenFilter: true  );
	IndirectEquityInvestment: Boolean @( title: '{i18n>FinancialContract.IndirectEquityInvestment}', UI.HiddenFilter: true  );
	InflationIndexLag: Decimal(34,6) @( title: '{i18n>FinancialContract.InflationIndexLag}', UI.HiddenFilter: true  );
	InflationIndexLagUnit: String(12) @( title: '{i18n>FinancialContract.InflationIndexLagUnit}', UI.HiddenFilter: true  );
	InflationIndexRoundingPrecision: Integer @( title: '{i18n>FinancialContract.InflationIndexRoundingPrecision}', UI.HiddenFilter: true  );
	InflationIndexValueConvention: String(40) @( title: '{i18n>FinancialContract.InflationIndexValueConvention}', UI.HiddenFilter: true  );
	InflationOptionType: String(40) @( title: '{i18n>FinancialContract.InflationOptionType}', UI.HiddenFilter: true  );
	InitialMargin: Decimal(15,11) @( title: '{i18n>FinancialContract.InitialMargin}', UI.HiddenFilter: true  );
	InitialPaymentAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.InitialPaymentAmount}', UI.HiddenFilter: true  );
	InitialPaymentCurrency: String(3) @( title: '{i18n>FinancialContract.InitialPaymentCurrency}', UI.HiddenFilter: true  );
	InitialPaymentPayer: String(50) @( title: '{i18n>FinancialContract.InitialPaymentPayer}', UI.HiddenFilter: true  );
	InsuranceContractEndDate: Date @( title: '{i18n>FinancialContract.InsuranceContractEndDate}', UI.HiddenFilter: true  );
	InsuranceContractEndTime: Time @( title: '{i18n>FinancialContract.InsuranceContractEndTime}', UI.HiddenFilter: true  );
	InsuranceContractEndTimeZoneCode: String(100) @( title: '{i18n>FinancialContract.InsuranceContractEndTimeZoneCode}', UI.HiddenFilter: true  );
	InsuranceContractStartDate: Date @( title: '{i18n>FinancialContract.InsuranceContractStartDate}', UI.HiddenFilter: true  );
	InsuranceContractStartTime: Time @( title: '{i18n>FinancialContract.InsuranceContractStartTime}', UI.HiddenFilter: true  );
	InsuranceContractStartTimeZoneCode: String(100) @( title: '{i18n>FinancialContract.InsuranceContractStartTimeZoneCode}', UI.HiddenFilter: true  );
	InsuranceTaxExemptionType: String(40) @( title: '{i18n>FinancialContract.InsuranceTaxExemptionType}', UI.HiddenFilter: true  );
	InsuredLoanAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.InsuredLoanAmount}', UI.HiddenFilter: true  );
	IntendedAssetLiability: String(40) @( title: '{i18n>FinancialContract.IntendedAssetLiability}', UI.HiddenFilter: true  );
	InterestRateOptionType: String(40) @( title: '{i18n>FinancialContract.InterestRateOptionType}', UI.HiddenFilter: true  );
	InterestRateSwapCategory: String(40) @( title: '{i18n>FinancialContract.InterestRateSwapCategory}', UI.HiddenFilter: true  );
	InterimPrincipalExchange: Boolean @( title: '{i18n>FinancialContract.InterimPrincipalExchange}', UI.HiddenFilter: true  );
	Interpolation: Boolean @( title: '{i18n>FinancialContract.Interpolation}', UI.HiddenFilter: true  );
	InterpolationFrequency: String(40) @( title: '{i18n>FinancialContract.InterpolationFrequency}', UI.HiddenFilter: true  );
	InterpolationMethod: String(100) @( title: '{i18n>FinancialContract.InterpolationMethod}', UI.HiddenFilter: true  );
	InterpolationRoundingPrecision: Integer @( title: '{i18n>FinancialContract.InterpolationRoundingPrecision}', UI.HiddenFilter: true  );
	IntraGroupRetroCessionType: String(40) @( title: '{i18n>FinancialContract.IntraGroupRetroCessionType}', UI.HiddenFilter: true  );
	InvoiceDiscountingAgreementType: String(100) @( title: '{i18n>FinancialContract.InvoiceDiscountingAgreementType}', UI.HiddenFilter: true  );
	IsBankruptcyRemote: Boolean @( title: '{i18n>FinancialContract.IsBankruptcyRemote}', UI.HiddenFilter: true  );
	IsCorporateCard: Boolean @( title: '{i18n>FinancialContract.IsCorporateCard}', UI.HiddenFilter: true  );
	IsLegallyUncertain: Boolean @( title: '{i18n>FinancialContract.IsLegallyUncertain}', UI.HiddenFilter: true  );
	IsNegotiable: Boolean @( title: '{i18n>FinancialContract.IsNegotiable}', UI.HiddenFilter: true  );
	IsStandby: Boolean @( title: '{i18n>FinancialContract.IsStandby}', UI.HiddenFilter: true  );
	IsSubleaseAllowed: Boolean @( title: '{i18n>FinancialContract.IsSubleaseAllowed}', UI.HiddenFilter: true  );
	IssueDate: Date @( title: '{i18n>FinancialContract.IssueDate}', UI.HiddenFilter: true  );
	IssueRegion: String(50) @( title: '{i18n>FinancialContract.IssueRegion}', UI.HiddenFilter: true  );
	LatestIssueDate: Date @( title: '{i18n>FinancialContract.LatestIssueDate}', UI.HiddenFilter: true  );
	LatestShipmentDate: Date @( title: '{i18n>FinancialContract.LatestShipmentDate}', UI.HiddenFilter: true  );
	LeaseTermEnd: Date @( title: '{i18n>FinancialContract.LeaseTermEnd}', UI.HiddenFilter: true  );
	LeaseTermStart: Date @( title: '{i18n>FinancialContract.LeaseTermStart}', UI.HiddenFilter: true  );
	Leg1NotionalAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.Leg1NotionalAmount}', UI.HiddenFilter: true  );
	Leg1NotionalAmountCurrency: String(3) @( title: '{i18n>FinancialContract.Leg1NotionalAmountCurrency}', UI.HiddenFilter: true  );
	Leg2AgainstLeg1FXRate: Decimal(34,6) @( title: '{i18n>FinancialContract.Leg2AgainstLeg1FXRate}', UI.HiddenFilter: true  );
	Leg2NotionalAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.Leg2NotionalAmount}', UI.HiddenFilter: true  );
	Leg2NotionalAmountCurrency: String(3) @( title: '{i18n>FinancialContract.Leg2NotionalAmountCurrency}', UI.HiddenFilter: true  );
	LegWithAdjustedPrincipal: Integer @( title: '{i18n>FinancialContract.LegWithAdjustedPrincipal}', UI.HiddenFilter: true  );
	LetterOfCreditCategory: String(24) @( title: '{i18n>FinancialContract.LetterOfCreditCategory}', UI.HiddenFilter: true  );
	LetterOfCreditType: String(50) @( title: '{i18n>FinancialContract.LetterOfCreditType}', UI.HiddenFilter: true  );
	LifeAndAnnuityInsuranceContractCategory: String(40) @( title: '{i18n>FinancialContract.LifeAndAnnuityInsuranceContractCategory}', UI.HiddenFilter: true  );
	LifeAndAnnuityNatureCode: String(40) @( title: '{i18n>FinancialContract.LifeAndAnnuityNatureCode}', UI.HiddenFilter: true  );
	LifecycleStatus: String(40) @( title: '{i18n>FinancialContract.LifecycleStatus}', UI.HiddenFilter: true  );
	LifecycleStatusChangeDate: Date @( title: '{i18n>FinancialContract.LifecycleStatusChangeDate}', UI.HiddenFilter: true  );
	LifecycleStatusReason: String(256) @( title: '{i18n>FinancialContract.LifecycleStatusReason}', UI.HiddenFilter: true  );
	LoanIncreaseFactor: Decimal(15,11) @( title: '{i18n>FinancialContract.LoanIncreaseFactor}', UI.HiddenFilter: true  );
	LoanInsuranceProtectionLevel: String(10) @( title: '{i18n>FinancialContract.LoanInsuranceProtectionLevel}', UI.HiddenFilter: true  );
	LockerClientKeyWithBank: Boolean @( title: '{i18n>FinancialContract.LockerClientKeyWithBank}', UI.HiddenFilter: true  );
	LockerKeyID: String(100) @( title: '{i18n>FinancialContract.LockerKeyID}', UI.HiddenFilter: true  );
	ManufacturedPayments: Boolean @( title: '{i18n>FinancialContract.ManufacturedPayments}', UI.HiddenFilter: true  );
	MarginType: String(40) @( title: '{i18n>FinancialContract.MarginType}', UI.HiddenFilter: true  );
	MasterAgreementCategory: String(40) @( title: '{i18n>FinancialContract.MasterAgreementCategory}', UI.HiddenFilter: true  );
	MaterialStartDate: Date @( title: '{i18n>FinancialContract.MaterialStartDate}', UI.HiddenFilter: true  );
	MaterialStartTime: Time @( title: '{i18n>FinancialContract.MaterialStartTime}', UI.HiddenFilter: true  );
	MaterialStartTimeZone: String(100) @( title: '{i18n>FinancialContract.MaterialStartTimeZone}', UI.HiddenFilter: true  );
	MaturityDate: Date @( title: '{i18n>FinancialContract.MaturityDate}', UI.HiddenFilter: true  );
	MaximumCollateralAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.MaximumCollateralAmount}', UI.HiddenFilter: true  );
	MaximumCollateralAmountCurrency: String(3) @( title: '{i18n>FinancialContract.MaximumCollateralAmountCurrency}', UI.HiddenFilter: true  );
	MaximumNotionalAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.MaximumNotionalAmount}', UI.HiddenFilter: true  );
	MinimumContractTermPeriodLength: Decimal(34,6) @( title: '{i18n>FinancialContract.MinimumContractTermPeriodLength}', UI.HiddenFilter: true  );
	MinimumContractTermTimeUnit: String(12) @( title: '{i18n>FinancialContract.MinimumContractTermTimeUnit}', UI.HiddenFilter: true  );
	MinimumSavingsAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.MinimumSavingsAmount}', UI.HiddenFilter: true  );
	MinimumSavingsAmountCurrency: String(3) @( title: '{i18n>FinancialContract.MinimumSavingsAmountCurrency}', UI.HiddenFilter: true  );
	MinimumSavingsTermPeriodLength: Decimal(34,6) @( title: '{i18n>FinancialContract.MinimumSavingsTermPeriodLength}', UI.HiddenFilter: true  );
	MinimumSavingsTermTimeUnit: String(12) @( title: '{i18n>FinancialContract.MinimumSavingsTermTimeUnit}', UI.HiddenFilter: true  );
	MinimumSettlementPercentage: Decimal(15,11) @( title: '{i18n>FinancialContract.MinimumSettlementPercentage}', UI.HiddenFilter: true  );
	MinimumValuationIndex: Integer @( title: '{i18n>FinancialContract.MinimumValuationIndex}', UI.HiddenFilter: true  );
	MostSeniorTranche: Boolean @( title: '{i18n>FinancialContract.MostSeniorTranche}', UI.HiddenFilter: true  );
	MultilinePolicyCategory: String(40) @( title: '{i18n>FinancialContract.MultilinePolicyCategory}', UI.HiddenFilter: true  );
	NOutOfMToDefault: Integer @( title: '{i18n>FinancialContract.NOutOfMToDefault}', UI.HiddenFilter: true  );
	NToDefault: Integer @( title: '{i18n>FinancialContract.NToDefault}', UI.HiddenFilter: true  );
	NatureCode: String(40) @( title: '{i18n>FinancialContract.NatureCode}', UI.HiddenFilter: true  );
	NetPayingSecuritiesAllowed: Boolean @( title: '{i18n>FinancialContract.NetPayingSecuritiesAllowed}', UI.HiddenFilter: true  );
	NettedSettlement: Boolean @( title: '{i18n>FinancialContract.NettedSettlement}', UI.HiddenFilter: true  );
	NettingPeriodEndDate: Date @( title: '{i18n>FinancialContract.NettingPeriodEndDate}', UI.HiddenFilter: true  );
	NettingPeriodStartDate: Date @( title: '{i18n>FinancialContract.NettingPeriodStartDate}', UI.HiddenFilter: true  );
	NettingProvision: Boolean @( title: '{i18n>FinancialContract.NettingProvision}', UI.HiddenFilter: true  );
	NewPolicyCancellationDate: Date @( title: '{i18n>FinancialContract.NewPolicyCancellationDate}', UI.HiddenFilter: true  );
	NominalAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.NominalAmount}', UI.HiddenFilter: true  );
	NominalAmountCurrency: String(3) @( title: '{i18n>FinancialContract.NominalAmountCurrency}', UI.HiddenFilter: true  );
	NominalAmountOfInstrumentBorrowed: Decimal(34,6) @( title: '{i18n>FinancialContract.NominalAmountOfInstrumentBorrowed}', UI.HiddenFilter: true  );
	NominalAmountOfInstrumentLent: Decimal(34,6) @( title: '{i18n>FinancialContract.NominalAmountOfInstrumentLent}', UI.HiddenFilter: true  );
	NominalAmountOfInstrumentsSold: Decimal(34,6) @( title: '{i18n>FinancialContract.NominalAmountOfInstrumentsSold}', UI.HiddenFilter: true  );
	NoticePeriodLength: Decimal(34,6) @( title: '{i18n>FinancialContract.NoticePeriodLength}', UI.HiddenFilter: true  );
	NoticePeriodTimeUnit: String(12) @( title: '{i18n>FinancialContract.NoticePeriodTimeUnit}', UI.HiddenFilter: true  );
	NotionalAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.NotionalAmount}', UI.HiddenFilter: true  );
	NotionalAmountCurrency: String(3) @( title: '{i18n>FinancialContract.NotionalAmountCurrency}', UI.HiddenFilter: true  );
	NotionalCashPoolingIndicator: Boolean @( title: '{i18n>FinancialContract.NotionalCashPoolingIndicator}', UI.HiddenFilter: true  );
	NotionalInTradedCurrency: Decimal(34,6) @( title: '{i18n>FinancialContract.NotionalInTradedCurrency}', UI.HiddenFilter: true  );
	NotionalQuantity: Decimal(34,6) @( title: '{i18n>FinancialContract.NotionalQuantity}', UI.HiddenFilter: true  );
	NotionalQuantityUnit: String(10) @( title: '{i18n>FinancialContract.NotionalQuantityUnit}', UI.HiddenFilter: true  );
	OTCDerivativeContractCategory: String(40) @( title: '{i18n>FinancialContract.OTCDerivativeContractCategory}', UI.HiddenFilter: true  );
	OffShoreIndicator: Boolean @( title: '{i18n>FinancialContract.OffShoreIndicator}', UI.HiddenFilter: true  );
	OfferValidityEndDate: Date @( title: '{i18n>FinancialContract.OfferValidityEndDate}', UI.HiddenFilter: true  );
	OfferValidityStartDate: Date @( title: '{i18n>FinancialContract.OfferValidityStartDate}', UI.HiddenFilter: true  );
	OpenMaturity: Boolean @( title: '{i18n>FinancialContract.OpenMaturity}', UI.HiddenFilter: true  );
	OpeningDate: Date @( title: '{i18n>FinancialContract.OpeningDate}', UI.HiddenFilter: true  );
	OptionCategory: String(40) @( title: '{i18n>FinancialContract.OptionCategory}', UI.HiddenFilter: true  );
	OptionMarginingStyle: String(30) @( title: '{i18n>FinancialContract.OptionMarginingStyle}', UI.HiddenFilter: true  );
	OptionPremium: Decimal(34,6) @( title: '{i18n>FinancialContract.OptionPremium}', UI.HiddenFilter: true  );
	OptionPremiumCurrency: String(3) @( title: '{i18n>FinancialContract.OptionPremiumCurrency}', UI.HiddenFilter: true  );
	OptionPremiumPaymentDate: Date @( title: '{i18n>FinancialContract.OptionPremiumPaymentDate}', UI.HiddenFilter: true  );
	OptionPrice: Decimal(34,6) @( title: '{i18n>FinancialContract.OptionPrice}', UI.HiddenFilter: true  );
	OptionPriceCurrency: String(3) @( title: '{i18n>FinancialContract.OptionPriceCurrency}', UI.HiddenFilter: true  );
	OriginalDepositEndDate: Date @( title: '{i18n>FinancialContract.OriginalDepositEndDate}', UI.HiddenFilter: true  );
	OriginalInvestmentAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.OriginalInvestmentAmount}', UI.HiddenFilter: true  );
	OriginalNotionalAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.OriginalNotionalAmount}', UI.HiddenFilter: true  );
	OriginalSigningDate: Date @( title: '{i18n>FinancialContract.OriginalSigningDate}', UI.HiddenFilter: true  );
	OverpaymentPermitted: Boolean @( title: '{i18n>FinancialContract.OverpaymentPermitted}', UI.HiddenFilter: true  );
	POCIAcquisitionStatus: String(40) @( title: '{i18n>FinancialContract.POCIAcquisitionStatus}', UI.HiddenFilter: true  );
	PartialDrawingsPermitted: Boolean @( title: '{i18n>FinancialContract.PartialDrawingsPermitted}', UI.HiddenFilter: true  );
	PartialPaymentMethod: String(20) @( title: '{i18n>FinancialContract.PartialPaymentMethod}', UI.HiddenFilter: true  );
	PartialShipmentAllowed: Boolean @( title: '{i18n>FinancialContract.PartialShipmentAllowed}', UI.HiddenFilter: true  );
	ParticipationEndDate: Date @( title: '{i18n>FinancialContract.ParticipationEndDate}', UI.HiddenFilter: true  );
	ParticipationStartDate: Date @( title: '{i18n>FinancialContract.ParticipationStartDate}', UI.HiddenFilter: true  );
	PayrollAccount: Boolean @( title: '{i18n>FinancialContract.PayrollAccount}', UI.HiddenFilter: true  );
	PlaceOfJurisdiction: String(256) @( title: '{i18n>FinancialContract.PlaceOfJurisdiction}', UI.HiddenFilter: true  );
	PolicyholderOriginCountry: String(2) @( title: '{i18n>FinancialContract.PolicyholderOriginCountry}', UI.HiddenFilter: true  );
	PoolFactor: Decimal(15,11) @( title: '{i18n>FinancialContract.PoolFactor}', UI.HiddenFilter: true  );
	PremiumAnnualTaxAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.PremiumAnnualTaxAmount}', UI.HiddenFilter: true  );
	PremiumBenefitCalculationRule: String(40) @( title: '{i18n>FinancialContract.PremiumBenefitCalculationRule}', UI.HiddenFilter: true  );
	PremiumGrossAnnualAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.PremiumGrossAnnualAmount}', UI.HiddenFilter: true  );
	PremiumGrossPeriodAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.PremiumGrossPeriodAmount}', UI.HiddenFilter: true  );
	PremiumPaymentFrequency: String(40) @( title: '{i18n>FinancialContract.PremiumPaymentFrequency}', UI.HiddenFilter: true  );
	PremiumPaymentMode: String(40) @( title: '{i18n>FinancialContract.PremiumPaymentMode}', UI.HiddenFilter: true  );
	PresentationPeriod: Decimal(34,6) @( title: '{i18n>FinancialContract.PresentationPeriod}', UI.HiddenFilter: true  );
	PresentationPeriodUnit: String(12) @( title: '{i18n>FinancialContract.PresentationPeriodUnit}', UI.HiddenFilter: true  );
	PricePerTradedCurrencyUnit: Decimal(34,6) @( title: '{i18n>FinancialContract.PricePerTradedCurrencyUnit}', UI.HiddenFilter: true  );
	PricePerTradedCurrencyUnitFirstSettlement: Decimal(34,6) @( title: '{i18n>FinancialContract.PricePerTradedCurrencyUnitFirstSettlement}', UI.HiddenFilter: true  );
	PricePerTradedCurrencyUnitSecondSettlement: Decimal(34,6) @( title: '{i18n>FinancialContract.PricePerTradedCurrencyUnitSecondSettlement}', UI.HiddenFilter: true  );
	PriceQuotationType: String(40) @( title: '{i18n>FinancialContract.PriceQuotationType}', UI.HiddenFilter: true  );
	PrincipalAdjustedForFXRate: Boolean @( title: '{i18n>FinancialContract.PrincipalAdjustedForFXRate}', UI.HiddenFilter: true  );
	PrincipalAdjustmentLagLength: Integer @( title: '{i18n>FinancialContract.PrincipalAdjustmentLagLength}', UI.HiddenFilter: true  );
	PrincipalExchangeAtEnd: Boolean @( title: '{i18n>FinancialContract.PrincipalExchangeAtEnd}', UI.HiddenFilter: true  );
	PrincipalExchangeAtStart: Boolean @( title: '{i18n>FinancialContract.PrincipalExchangeAtStart}', UI.HiddenFilter: true  );
	ProfitTransferAgreementExists: Boolean @( title: '{i18n>FinancialContract.ProfitTransferAgreementExists}', UI.HiddenFilter: true  );
	PromotionalLoanProgramID: String(100) @( title: '{i18n>FinancialContract.PromotionalLoanProgramID}', UI.HiddenFilter: true  );
	PropertyandCasualtyContractCategory: String(40) @( title: '{i18n>FinancialContract.PropertyandCasualtyContractCategory}', UI.HiddenFilter: true  );
	PurchaseDate: Date @( title: '{i18n>FinancialContract.PurchaseDate}', UI.HiddenFilter: true  );
	PurchasePricePerInstrument: Decimal(34,6) @( title: '{i18n>FinancialContract.PurchasePricePerInstrument}', UI.HiddenFilter: true  );
	Purpose: String(256) @( title: '{i18n>FinancialContract.Purpose}', UI.HiddenFilter: true  );
	PurposeType: String(60) @( title: '{i18n>FinancialContract.PurposeType}', UI.HiddenFilter: true  );
	PutOrCall: String(10) @( title: '{i18n>FinancialContract.PutOrCall}', UI.HiddenFilter: true  );
	QLACIndicator: Boolean @( title: '{i18n>FinancialContract.QLACIndicator}', UI.HiddenFilter: true  );
	Quantity: Decimal(34,6) @( title: '{i18n>FinancialContract.Quantity}', UI.HiddenFilter: true  );
	QuantityBorrowed: Decimal(34,6) @( title: '{i18n>FinancialContract.QuantityBorrowed}', UI.HiddenFilter: true  );
	QuantityLent: Decimal(34,6) @( title: '{i18n>FinancialContract.QuantityLent}', UI.HiddenFilter: true  );
	QuantitySold: Decimal(34,6) @( title: '{i18n>FinancialContract.QuantitySold}', UI.HiddenFilter: true  );
	QuotationType: String(40) @( title: '{i18n>FinancialContract.QuotationType}', UI.HiddenFilter: true  );
	QuoteCurrency: String(3) @( title: '{i18n>FinancialContract.QuoteCurrency}', UI.HiddenFilter: true  );
	RMDLifeExpancy: Decimal(4,1) @( title: '{i18n>FinancialContract.RMDLifeExpancy}', UI.HiddenFilter: true  );
	RMDLifeExpectancyTable: String(40) @( title: '{i18n>FinancialContract.RMDLifeExpectancyTable}', UI.HiddenFilter: true  );
	RankAgainstOtherCollateralAgreements: Integer @( title: '{i18n>FinancialContract.RankAgainstOtherCollateralAgreements}', UI.HiddenFilter: true  );
	RateRoundingPrecision: Integer @( title: '{i18n>FinancialContract.RateRoundingPrecision}', UI.HiddenFilter: true  );
	ReceiptDate: Date @( title: '{i18n>FinancialContract.ReceiptDate}', UI.HiddenFilter: true  );
	ReceivablePurchaseAgreementCategory: String(100) @( title: '{i18n>FinancialContract.ReceivablePurchaseAgreementCategory}', UI.HiddenFilter: true  );
	Recourse: Boolean @( title: '{i18n>FinancialContract.Recourse}', UI.HiddenFilter: true  );
	RecoursePercentage: Decimal(15,11) @( title: '{i18n>FinancialContract.RecoursePercentage}', UI.HiddenFilter: true  );
	ReferenceMasterAgreement: String(256) @( title: '{i18n>FinancialContract.ReferenceMasterAgreement}', UI.HiddenFilter: true  );
	RepoInterestAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.RepoInterestAmount}', UI.HiddenFilter: true  );
	RepoToMaturity: Boolean @( title: '{i18n>FinancialContract.RepoToMaturity}', UI.HiddenFilter: true  );
	RepurchaseDate: Date @( title: '{i18n>FinancialContract.RepurchaseDate}', UI.HiddenFilter: true  );
	RepurchasePricePerInstrument: Decimal(34,6) @( title: '{i18n>FinancialContract.RepurchasePricePerInstrument}', UI.HiddenFilter: true  );
	ResaleAllowed: Boolean @( title: '{i18n>FinancialContract.ResaleAllowed}', UI.HiddenFilter: true  );
	Resecuritization: Boolean @( title: '{i18n>FinancialContract.Resecuritization}', UI.HiddenFilter: true  );
	ResidualValue: Decimal(34,6) @( title: '{i18n>FinancialContract.ResidualValue}', UI.HiddenFilter: true  );
	ResidualValueCurrency: String(3) @( title: '{i18n>FinancialContract.ResidualValueCurrency}', UI.HiddenFilter: true  );
	Revocable: Boolean @( title: '{i18n>FinancialContract.Revocable}', UI.HiddenFilter: true  );
	RiskCountry: String(2) @( title: '{i18n>FinancialContract.RiskCountry}', UI.HiddenFilter: true  );
	RiskRetentionHolder: String(50) @( title: '{i18n>FinancialContract.RiskRetentionHolder}', UI.HiddenFilter: true  );
	RiskRetentionMethod: String(100) @( title: '{i18n>FinancialContract.RiskRetentionMethod}', UI.HiddenFilter: true  );
	RiskRetentionRate: Decimal(15,11) @( title: '{i18n>FinancialContract.RiskRetentionRate}', UI.HiddenFilter: true  );
	RiskStatus: String(40) @( title: '{i18n>FinancialContract.RiskStatus}', UI.HiddenFilter: true  );
	RoundingMethodAmount: String(20) @( title: '{i18n>FinancialContract.RoundingMethodAmount}', UI.HiddenFilter: true  );
	RoundingMethodInterpolation: String(20) @( title: '{i18n>FinancialContract.RoundingMethodInterpolation}', UI.HiddenFilter: true  );
	RoundingMethodReferenceRate: String(10) @( title: '{i18n>FinancialContract.RoundingMethodReferenceRate}', UI.HiddenFilter: true  );
	SWIFT: String(128) @( title: '{i18n>FinancialContract.SWIFT}', UI.HiddenFilter: true  );
	SavingsRate: Decimal(15,11) @( title: '{i18n>FinancialContract.SavingsRate}', UI.HiddenFilter: true  );
	SavingsSchemeCurrency: String(3) @( title: '{i18n>FinancialContract.SavingsSchemeCurrency}', UI.HiddenFilter: true  );
	SecondSettlementDate: Date @( title: '{i18n>FinancialContract.SecondSettlementDate}', UI.HiddenFilter: true  );
	SecuritiesLendingEndDate: Date @( title: '{i18n>FinancialContract.SecuritiesLendingEndDate}', UI.HiddenFilter: true  );
	SecuritiesLendingStartDate: Date @( title: '{i18n>FinancialContract.SecuritiesLendingStartDate}', UI.HiddenFilter: true  );
	SecuritizationName: String(256) @( title: '{i18n>FinancialContract.SecuritizationName}', UI.HiddenFilter: true  );
	SecurityDeposit: Decimal(34,6) @( title: '{i18n>FinancialContract.SecurityDeposit}', UI.HiddenFilter: true  );
	SecurityDepositCurrency: String(3) @( title: '{i18n>FinancialContract.SecurityDepositCurrency}', UI.HiddenFilter: true  );
	SegregatedAccountBeneficiaryID: String(128) @( title: '{i18n>FinancialContract.SegregatedAccountBeneficiaryID}', UI.HiddenFilter: true  );
	SegregatedAccountBeneficiaryName: String(256) @( title: '{i18n>FinancialContract.SegregatedAccountBeneficiaryName}', UI.HiddenFilter: true  );
	SelfAdvised: Boolean @( title: '{i18n>FinancialContract.SelfAdvised}', UI.HiddenFilter: true  );
	SellBuyBackAllowed: Boolean @( title: '{i18n>FinancialContract.SellBuyBackAllowed}', UI.HiddenFilter: true  );
	SellerCanChooseCashSettlement: Boolean @( title: '{i18n>FinancialContract.SellerCanChooseCashSettlement}', UI.HiddenFilter: true  );
	Seniority: String(40) @( title: '{i18n>FinancialContract.Seniority}', UI.HiddenFilter: true  );
	ServiceAgreementCategory: String(50) @( title: '{i18n>FinancialContract.ServiceAgreementCategory}', UI.HiddenFilter: true  );
	SettlementAccountManagedByThirdParty: Boolean @( title: '{i18n>FinancialContract.SettlementAccountManagedByThirdParty}', UI.HiddenFilter: true  );
	SettlementAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.SettlementAmount}', UI.HiddenFilter: true  );
	SettlementBusinessCalendar: String(50) @( title: '{i18n>FinancialContract.SettlementBusinessCalendar}', UI.HiddenFilter: true  );
	SettlementBusinessDayConvention: String(40) @( title: '{i18n>FinancialContract.SettlementBusinessDayConvention}', UI.HiddenFilter: true  );
	SettlementCurrency: String(3) @( title: '{i18n>FinancialContract.SettlementCurrency}', UI.HiddenFilter: true  );
	SettlementDate: Date @( title: '{i18n>FinancialContract.SettlementDate}', UI.HiddenFilter: true  );
	SettlementDirectDebitStatus: String(100) @( title: '{i18n>FinancialContract.SettlementDirectDebitStatus}', UI.HiddenFilter: true  );
	SettlementFXRate: Decimal(34,6) @( title: '{i18n>FinancialContract.SettlementFXRate}', UI.HiddenFilter: true  );
	SettlementInArrears: Boolean @( title: '{i18n>FinancialContract.SettlementInArrears}', UI.HiddenFilter: true  );
	SettlementMethod: String(20) @( title: '{i18n>FinancialContract.SettlementMethod}', UI.HiddenFilter: true  );
	SettlementNettingIndicator: Boolean @( title: '{i18n>FinancialContract.SettlementNettingIndicator}', UI.HiddenFilter: true  );
	SettlementNettingPeriod: Decimal(34,6) @( title: '{i18n>FinancialContract.SettlementNettingPeriod}', UI.HiddenFilter: true  );
	SettlementNettingPeriodTimeUnit: String(12) @( title: '{i18n>FinancialContract.SettlementNettingPeriodTimeUnit}', UI.HiddenFilter: true  );
	SettlementPeriodLength: Integer @( title: '{i18n>FinancialContract.SettlementPeriodLength}', UI.HiddenFilter: true  );
	SettlementPeriodTimeUnit: String(10) @( title: '{i18n>FinancialContract.SettlementPeriodTimeUnit}', UI.HiddenFilter: true  );
	SignatureDate: Date @( title: '{i18n>FinancialContract.SignatureDate}', UI.HiddenFilter: true  );
	SignatureMethod: String(40) @( title: '{i18n>FinancialContract.SignatureMethod}', UI.HiddenFilter: true  );
	SignificantRiskTransfer: Boolean @( title: '{i18n>FinancialContract.SignificantRiskTransfer}', UI.HiddenFilter: true  );
	SimpleCollateralCategory: String(40) @( title: '{i18n>FinancialContract.SimpleCollateralCategory}', UI.HiddenFilter: true  );
	SingleCurrencyAccountCategory: String(40) @( title: '{i18n>FinancialContract.SingleCurrencyAccountCategory}', UI.HiddenFilter: true  );
	SpecializedLendingType: String(100) @( title: '{i18n>FinancialContract.SpecializedLendingType}', UI.HiddenFilter: true  );
	SpecifiedPrice: String(100) @( title: '{i18n>FinancialContract.SpecifiedPrice}', UI.HiddenFilter: true  );
	SpecifiedPriceforLeg1: String(100) @( title: '{i18n>FinancialContract.SpecifiedPriceforLeg1}', UI.HiddenFilter: true  );
	SpecifiedPriceforLeg2: String(100) @( title: '{i18n>FinancialContract.SpecifiedPriceforLeg2}', UI.HiddenFilter: true  );
	SpreadOverBenchmark: Decimal(15,11) @( title: '{i18n>FinancialContract.SpreadOverBenchmark}', UI.HiddenFilter: true  );
	StartDate: Date @( title: '{i18n>FinancialContract.StartDate}', UI.HiddenFilter: true  );
	StartOfCedentAccountingPeriodDate: Date @( title: '{i18n>FinancialContract.StartOfCedentAccountingPeriodDate}', UI.HiddenFilter: true  );
	StatementofAccountFrequency: String(40) @( title: '{i18n>FinancialContract.StatementofAccountFrequency}', UI.HiddenFilter: true  );
	StrikePrice: Decimal(34,6) @( title: '{i18n>FinancialContract.StrikePrice}', UI.HiddenFilter: true  );
	StrikePriceCurrency: String(3) @( title: '{i18n>FinancialContract.StrikePriceCurrency}', UI.HiddenFilter: true  );
	StrikePriceInQuotePerUnitTraded: Decimal(34,6) @( title: '{i18n>FinancialContract.StrikePriceInQuotePerUnitTraded}', UI.HiddenFilter: true  );
	StructuredProductSublegCategory: String(40) @( title: '{i18n>FinancialContract.StructuredProductSublegCategory}', UI.HiddenFilter: true  );
	SubmissionChannel: String(40) @( title: '{i18n>FinancialContract.SubmissionChannel}', UI.HiddenFilter: true  );
	SwapCategory: String(40) @( title: '{i18n>FinancialContract.SwapCategory}', UI.HiddenFilter: true  );
	SwaptionDirection: String(20) @( title: '{i18n>FinancialContract.SwaptionDirection}', UI.HiddenFilter: true  );
	SwaptionUnderlyingCategory: String(40) @( title: '{i18n>FinancialContract.SwaptionUnderlyingCategory}', UI.HiddenFilter: true  );
	SyndicatedContractIdentifier: String(128) @( title: '{i18n>FinancialContract.SyndicatedContractIdentifier}', UI.HiddenFilter: true  );
	SyndicatedRefinancingIsPermitted: Boolean @( title: '{i18n>FinancialContract.SyndicatedRefinancingIsPermitted}', UI.HiddenFilter: true  );
	SyndicationAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.SyndicationAmount}', UI.HiddenFilter: true  );
	SyndicationAmountCurrency: String(3) @( title: '{i18n>FinancialContract.SyndicationAmountCurrency}', UI.HiddenFilter: true  );
	TariffAnnualAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.TariffAnnualAmount}', UI.HiddenFilter: true  );
	TariffPeriodAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.TariffPeriodAmount}', UI.HiddenFilter: true  );
	TechnicalAccountingLevelCode: String(40) @( title: '{i18n>FinancialContract.TechnicalAccountingLevelCode}', UI.HiddenFilter: true  );
	TerminationDate: Date @( title: '{i18n>FinancialContract.TerminationDate}', UI.HiddenFilter: true  );
	TotalFinancingAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.TotalFinancingAmount}', UI.HiddenFilter: true  );
	TotalNotionalQuantity: Decimal(34,6) @( title: '{i18n>FinancialContract.TotalNotionalQuantity}', UI.HiddenFilter: true  );
	TotalNotionalQuantityUnit: String(10) @( title: '{i18n>FinancialContract.TotalNotionalQuantityUnit}', UI.HiddenFilter: true  );
	TotalPurchasePrice: Decimal(34,6) @( title: '{i18n>FinancialContract.TotalPurchasePrice}', UI.HiddenFilter: true  );
	TotalRepayableAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.TotalRepayableAmount}', UI.HiddenFilter: true  );
	TotalRepayableAmountCurrency: String(3) @( title: '{i18n>FinancialContract.TotalRepayableAmountCurrency}', UI.HiddenFilter: true  );
	TotalRepurchasePrice: Decimal(34,6) @( title: '{i18n>FinancialContract.TotalRepurchasePrice}', UI.HiddenFilter: true  );
	TotalReturnSwapCategory: String(40) @( title: '{i18n>FinancialContract.TotalReturnSwapCategory}', UI.HiddenFilter: true  );
	TradedCurrency: String(3) @( title: '{i18n>FinancialContract.TradedCurrency}', UI.HiddenFilter: true  );
	TransShipmentAllowed: Boolean @( title: '{i18n>FinancialContract.TransShipmentAllowed}', UI.HiddenFilter: true  );
	TransactionalAccount: Boolean @( title: '{i18n>FinancialContract.TransactionalAccount}', UI.HiddenFilter: true  );
	TypeOfPledge: String(40) @( title: '{i18n>FinancialContract.TypeOfPledge}', UI.HiddenFilter: true  );
	TypeOfSyndicate: String(40) @( title: '{i18n>FinancialContract.TypeOfSyndicate}', UI.HiddenFilter: true  );
	TypeOfSyndication: String(40) @( title: '{i18n>FinancialContract.TypeOfSyndication}', UI.HiddenFilter: true  );
	UnderlyingEffectiveDate: Date @( title: '{i18n>FinancialContract.UnderlyingEffectiveDate}', UI.HiddenFilter: true  );
	UnderlyingInterimPrincipalExchange: Boolean @( title: '{i18n>FinancialContract.UnderlyingInterimPrincipalExchange}', UI.HiddenFilter: true  );
	UnderlyingLeg1NotionalAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.UnderlyingLeg1NotionalAmount}', UI.HiddenFilter: true  );
	UnderlyingLeg1NotionalAmountCurrency: String(3) @( title: '{i18n>FinancialContract.UnderlyingLeg1NotionalAmountCurrency}', UI.HiddenFilter: true  );
	UnderlyingLeg2AgainstLeg1FXRate: Decimal(34,6) @( title: '{i18n>FinancialContract.UnderlyingLeg2AgainstLeg1FXRate}', UI.HiddenFilter: true  );
	UnderlyingLeg2NotionalAmount: Decimal(34,6) @( title: '{i18n>FinancialContract.UnderlyingLeg2NotionalAmount}', UI.HiddenFilter: true  );
	UnderlyingLeg2NotionalAmountCurrency: String(3) @( title: '{i18n>FinancialContract.UnderlyingLeg2NotionalAmountCurrency}', UI.HiddenFilter: true  );
	UnderlyingLegWithAdjustedPrincipal: Integer @( title: '{i18n>FinancialContract.UnderlyingLegWithAdjustedPrincipal}', UI.HiddenFilter: true  );
	UnderlyingLegsHaveSameDirection: Boolean @( title: '{i18n>FinancialContract.UnderlyingLegsHaveSameDirection}', UI.HiddenFilter: true  );
	UnderlyingNettedSettlement: Boolean @( title: '{i18n>FinancialContract.UnderlyingNettedSettlement}', UI.HiddenFilter: true  );
	UnderlyingNotional: Decimal(34,6) @( title: '{i18n>FinancialContract.UnderlyingNotional}', UI.HiddenFilter: true  );
	UnderlyingNotionalCurrency: String(3) @( title: '{i18n>FinancialContract.UnderlyingNotionalCurrency}', UI.HiddenFilter: true  );
	UnderlyingPrincipalAdjustedForFXRate: Boolean @( title: '{i18n>FinancialContract.UnderlyingPrincipalAdjustedForFXRate}', UI.HiddenFilter: true  );
	UnderlyingPrincipalAdjustmentLagLength: Integer @( title: '{i18n>FinancialContract.UnderlyingPrincipalAdjustmentLagLength}', UI.HiddenFilter: true  );
	UnderlyingPrincipalExchangeAtEnd: Boolean @( title: '{i18n>FinancialContract.UnderlyingPrincipalExchangeAtEnd}', UI.HiddenFilter: true  );
	UnderlyingPrincipalExchangeAtStart: Boolean @( title: '{i18n>FinancialContract.UnderlyingPrincipalExchangeAtStart}', UI.HiddenFilter: true  );
	UnderlyingQuantity: Decimal(34,6) @( title: '{i18n>FinancialContract.UnderlyingQuantity}', UI.HiddenFilter: true  );
	UnderlyingSettlementCurrency: String(3) @( title: '{i18n>FinancialContract.UnderlyingSettlementCurrency}', UI.HiddenFilter: true  );
	UnderlyingSpecifiedPrice: String(100) @( title: '{i18n>FinancialContract.UnderlyingSpecifiedPrice}', UI.HiddenFilter: true  );
	UnderlyingTerminationDate: Date @( title: '{i18n>FinancialContract.UnderlyingTerminationDate}', UI.HiddenFilter: true  );
	UnderlyingTotalNotionalQuantity: Decimal(34,6) @( title: '{i18n>FinancialContract.UnderlyingTotalNotionalQuantity}', UI.HiddenFilter: true  );
	UnderlyingTotalNotionalQuantityUnit: String(10) @( title: '{i18n>FinancialContract.UnderlyingTotalNotionalQuantityUnit}', UI.HiddenFilter: true  );
	UnderwritingDate: Date @( title: '{i18n>FinancialContract.UnderwritingDate}', UI.HiddenFilter: true  );
	Unit: String(10) @( title: '{i18n>FinancialContract.Unit}', UI.HiddenFilter: true  );
	UnitOfQuantityBorrowed: String(10) @( title: '{i18n>FinancialContract.UnitOfQuantityBorrowed}', UI.HiddenFilter: true  );
	UnitofQuantityLent: String(10) @( title: '{i18n>FinancialContract.UnitofQuantityLent}', UI.HiddenFilter: true  );
	UseAsCollateralPermitted: Boolean @( title: '{i18n>FinancialContract.UseAsCollateralPermitted}', UI.HiddenFilter: true  );
	ValuationBusinessCalendar: String(50) @( title: '{i18n>FinancialContract.ValuationBusinessCalendar}', UI.HiddenFilter: true  );
	ValuationBusinessDayConvention: String(40) @( title: '{i18n>FinancialContract.ValuationBusinessDayConvention}', UI.HiddenFilter: true  );
	Version: String(256) @( title: '{i18n>FinancialContract.Version}', UI.HiddenFilter: true  );
	WithRecourse: Boolean @( title: '{i18n>FinancialContract.WithRecourse}', UI.HiddenFilter: true  );
	Yield: Decimal(15,11) @( title: '{i18n>FinancialContract.Yield}', UI.HiddenFilter: true  );
	isReinsuranceContract_ReinsuranceTechnicalCategoryCode: String(40) @( title: '{i18n>FinancialContract.isReinsuranceContract_ReinsuranceTechnicalCategoryCode}', UI.HiddenFilter: true  );
	SourceSystemID: String(128) @( title: '{i18n>FinancialContract.SourceSystemID}', UI.HiddenFilter: true  );
	ChangeTimestampInSourceSystem: Timestamp @( title: '{i18n>FinancialContract.ChangeTimestampInSourceSystem}', UI.HiddenFilter: true  );
	ChangingUserInSourceSystem: String(128) @( title: '{i18n>FinancialContract.ChangingUserInSourceSystem}', UI.HiddenFilter: true  );
	ChangingProcessType: String(40) @( title: '{i18n>FinancialContract.ChangingProcessType}', UI.HiddenFilter: true  );
	ChangingProcessID: String(128) @( title: '{i18n>FinancialContract.ChangingProcessID}', UI.HiddenFilter: true  );
	// Foreignkey Associations
	// child - parent associations: this entity is child
	ASSOC_CombinedCollateralAgreement_FinancialContractID: String(128) @(UI.HiddenFilter: false,
		Common.ValueList: {
			FetchValues: 1,
			CollectionPath: 'FinancialContract',
			Parameters: [
						{$Type: 'Common.ValueListParameterOut', LocalDataProperty: 'ASSOC_CombinedCollateralAgreement_FinancialContractID', ValueListProperty: 'FinancialContractID'},
				{$Type: 'Common.ValueListParameterOut', LocalDataProperty: 'ASSOC_CombinedCollateralAgreement_IDSystem', ValueListProperty: 'IDSystem'}
				,{ $Type: 'Common.ValueListParameterDisplayOnly', ValueListProperty: 'BusinessValidFrom' }
				,{ $Type: 'Common.ValueListParameterDisplayOnly', ValueListProperty: 'BusinessValidTo'}
			]
		});
	ASSOC_CombinedCollateralAgreement_IDSystem: String(40) @(UI.HiddenFilter: false);
	ASSOC_CombinedCollateralAgreement: association to _FinancialContract on ASSOC_CombinedCollateralAgreement.FinancialContractID = ASSOC_CombinedCollateralAgreement_FinancialContractID and ASSOC_CombinedCollateralAgreement.IDSystem = ASSOC_CombinedCollateralAgreement_IDSystem @(UI.HiddenFilter: false);
	ASSOC_FacilityOfDrawing_FinancialContractID: String(128) @(UI.HiddenFilter: false,
		Common.ValueList: {
			FetchValues: 1,
			CollectionPath: 'FinancialContract',
			Parameters: [
						{$Type: 'Common.ValueListParameterOut', LocalDataProperty: 'ASSOC_FacilityOfDrawing_FinancialContractID', ValueListProperty: 'FinancialContractID'},
				{$Type: 'Common.ValueListParameterOut', LocalDataProperty: 'ASSOC_FacilityOfDrawing_IDSystem', ValueListProperty: 'IDSystem'}
				,{ $Type: 'Common.ValueListParameterDisplayOnly', ValueListProperty: 'BusinessValidFrom' }
				,{ $Type: 'Common.ValueListParameterDisplayOnly', ValueListProperty: 'BusinessValidTo'}
			]
		});
	ASSOC_FacilityOfDrawing_IDSystem: String(40) @(UI.HiddenFilter: false);
	ASSOC_FacilityOfDrawing: association to _FinancialContract on ASSOC_FacilityOfDrawing.FinancialContractID = ASSOC_FacilityOfDrawing_FinancialContractID and ASSOC_FacilityOfDrawing.IDSystem = ASSOC_FacilityOfDrawing_IDSystem @(UI.HiddenFilter: false);
	ASSOC_FacilityOfSubFacility_FinancialContractID: String(128) @(UI.HiddenFilter: false,
		Common.ValueList: {
			FetchValues: 1,
			CollectionPath: 'FinancialContract',
			Parameters: [
						{$Type: 'Common.ValueListParameterOut', LocalDataProperty: 'ASSOC_FacilityOfSubFacility_FinancialContractID', ValueListProperty: 'FinancialContractID'},
				{$Type: 'Common.ValueListParameterOut', LocalDataProperty: 'ASSOC_FacilityOfSubFacility_IDSystem', ValueListProperty: 'IDSystem'}
				,{ $Type: 'Common.ValueListParameterDisplayOnly', ValueListProperty: 'BusinessValidFrom' }
				,{ $Type: 'Common.ValueListParameterDisplayOnly', ValueListProperty: 'BusinessValidTo'}
			]
		});
	ASSOC_FacilityOfSubFacility_IDSystem: String(40) @(UI.HiddenFilter: false);
	ASSOC_FacilityOfSubFacility: association to _FinancialContract on ASSOC_FacilityOfSubFacility.FinancialContractID = ASSOC_FacilityOfSubFacility_FinancialContractID and ASSOC_FacilityOfSubFacility.IDSystem = ASSOC_FacilityOfSubFacility_IDSystem @(UI.HiddenFilter: false);
	ASSOC_LocalSettlementAccountForCreditCard_FinancialContractID: String(128) @(UI.HiddenFilter: false,
		Common.ValueList: {
			FetchValues: 1,
			CollectionPath: 'FinancialContract',
			Parameters: [
						{$Type: 'Common.ValueListParameterOut', LocalDataProperty: 'ASSOC_LocalSettlementAccountForCreditCard_FinancialContractID', ValueListProperty: 'FinancialContractID'},
				{$Type: 'Common.ValueListParameterOut', LocalDataProperty: 'ASSOC_LocalSettlementAccountForCreditCard_IDSystem', ValueListProperty: 'IDSystem'}
				,{ $Type: 'Common.ValueListParameterDisplayOnly', ValueListProperty: 'BusinessValidFrom' }
				,{ $Type: 'Common.ValueListParameterDisplayOnly', ValueListProperty: 'BusinessValidTo'}
			]
		});
	ASSOC_LocalSettlementAccountForCreditCard_IDSystem: String(40) @(UI.HiddenFilter: false);
	ASSOC_LocalSettlementAccountForCreditCard: association to _FinancialContract on ASSOC_LocalSettlementAccountForCreditCard.FinancialContractID = ASSOC_LocalSettlementAccountForCreditCard_FinancialContractID and ASSOC_LocalSettlementAccountForCreditCard.IDSystem = ASSOC_LocalSettlementAccountForCreditCard_IDSystem @(UI.HiddenFilter: false);
	ASSOC_PhysicalAsset_PhysicalAssetID: String(168) @(UI.HiddenFilter: false);
	ASSOC_PhysicalAsset: association to _PhysicalAsset on ASSOC_PhysicalAsset.PhysicalAssetID = ASSOC_PhysicalAsset_PhysicalAssetID @(UI.HiddenFilter: false);
	ASSOC_SettlementAccountForDebitCard_FinancialContractID: String(128) @(UI.HiddenFilter: false,
		Common.ValueList: {
			FetchValues: 1,
			CollectionPath: 'FinancialContract',
			Parameters: [
						{$Type: 'Common.ValueListParameterOut', LocalDataProperty: 'ASSOC_SettlementAccountForDebitCard_FinancialContractID', ValueListProperty: 'FinancialContractID'},
				{$Type: 'Common.ValueListParameterOut', LocalDataProperty: 'ASSOC_SettlementAccountForDebitCard_IDSystem', ValueListProperty: 'IDSystem'}
				,{ $Type: 'Common.ValueListParameterDisplayOnly', ValueListProperty: 'BusinessValidFrom' }
				,{ $Type: 'Common.ValueListParameterDisplayOnly', ValueListProperty: 'BusinessValidTo'}
			]
		});
	ASSOC_SettlementAccountForDebitCard_IDSystem: String(40) @(UI.HiddenFilter: false);
	ASSOC_SettlementAccountForDebitCard: association to _FinancialContract on ASSOC_SettlementAccountForDebitCard.FinancialContractID = ASSOC_SettlementAccountForDebitCard_FinancialContractID and ASSOC_SettlementAccountForDebitCard.IDSystem = ASSOC_SettlementAccountForDebitCard_IDSystem @(UI.HiddenFilter: false);
    };

