using ClaimService as claim from './claim-service.cds';
annotate claim.Claim with @(UI : {
  SelectionFields                  : [
    BusinessPartnerID,
    BusinessValidFrom,
    BusinessValidTo
  ],
  //PresentationVariant #BusinessPartnerID   : {Visualizations : ['@UI.Chart#BusinessPartnerID', ], },
  PresentationVariant #Rating : {Visualizations : ['@UI.Chart#Rating'], },
  Chart                            : {
    ChartType           : #Column,
    Dimensions          : [BusinessPartnerID],
    DimensionAttributes : [{
      Dimension : BusinessPartnerID,
      Role      : #Category
    }],
    Measures            : [Amount],
    MeasureAttributes   : [{
      Measure : Amount,
      Role    : #Axis1
    }]
  },
  // Chart #BusinessPartnerID                 : {
  //   ChartType           : #Column,
  //   Dimensions          : [BusinessPartnerID],
  //   DimensionAttributes : [{
  //     Dimension : BusinessPartnerID,
  //     Role      : #Category
  //   }],
  //   Measures            : [Amount],
  //   MeasureAttributes   : [{
  //     Measure : Amount,
  //     Role    : #Axis1
  //   }]
  // },
  Chart#Rating                            : {
    ChartType           : #Column,
    Dimensions          : [Rating],
    DimensionAttributes : [{
      Dimension : Rating,
      Role      : #Category
    }],
    Measures            : [Amount],
    MeasureAttributes   : [{
      Measure : Amount,
      Role    : #Axis1
    }]
  },
  LineItem                         : [
    {Value : BusinessPartnerID},
    {Value : LoanID},
    {Value : IDSystem},
    {Value : BusinessValidFrom},
    {Value : BusinessValidTo},
    {Value : LastRatingDate},
    {Value : Status_code},
    {Value : Processed},
    {Value : Currency},
    {Value : Rating},
    {Value : Amount}
  ],
  HeaderInfo                       : {
    TypeName       : 'Claim',
    TypeNamePlural : 'Claims',
    Title          : {Value : BusinessPartnerID},
    Description    : {Value : LoanID}
  },
  Facets                           : [{
    $Type  : 'UI.ReferenceFacet',
    Label  : '{i18n>Details}',
    Target : '@UI.FieldGroup#Details'
  }, ],
  FieldGroup #Details              : {Data : [
    {Value : BusinessPartnerID},
    {Value : LoanID},
    {Value : IDSystem},
    {Value : BusinessValidFrom},
    {Value : BusinessValidTo},
    {Value : Amount},
    {Value : LastRatingDate},
    {Value : Status_code},
    {Value : Processed},
    {Value : Currency},
    {Value : Rating},
    {Value : BusinessPartnerClassification.ASSOC_ClassificationOfBusinessPartner_PartnerClass}
  ]}
});

annotate claim.Claim with {
    @Analytics.Dimension : true
    Rating;
    @Analytics.Dimension : true
    BusinessPartnerID;
    @Analytics.Dimension : true
    LastRatingDate;
};

annotate claim.Claim with {
   @Common : {ValueList #RatingVisualFilter : {
    $Type                        : 'Common.ValueListType',
    CollectionPath               : 'Claim',
    PresentationVariantQualifier : 'Rating',
    Parameters                   : [{
      $Type             : 'Common.ValueListParameterInOut',
      LocalDataProperty : 'Rating',
      ValueListProperty : 'Rating'
    }]
  }}
  Rating;
}

