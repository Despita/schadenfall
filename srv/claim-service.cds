using sf as _Claim from '../db/Claim-data-model';
using sf as _BP from '../db/BusinessPartner-data-model';

service ClaimService @(impl: './claim-service.js', path: 'Claim', requires: 'authenticated-user') {

	entity Claim @(
		Capabilities: {
			InsertRestrictions: {Insertable: true},
			UpdateRestrictions: {Updatable:  true},
			DeleteRestrictions: {Deletable:  true}
		},
		restrict: [ { grant: ['*'], to: 'admin' }, ]
	)
	as projection on _Claim.Claim
	actions{
		action checkRatings() returns Claim;
	};

    entity BusinessPartnerClassification as projection on _BP.BusinessPartnerClassification;
    entity BusinessPartner as projection on _BP.BusinessPartner;
}
