const cds = require('@sap/cds');
const LOG = cds.log('[Test - TestService]', cds.log.levels.TRACE);
const utility = require('./util/utility');
const { Test, Rating } = cds.entities('');

module.exports = (srv) => {
  srv.on('testAction', async (req) => {});

  srv.on('createNewRatings', async (req) => {
    //Create a new Rating for BusinessPartner 'BP_ID_009' with Rating I
    const rating = getDummyRating('I', 'BP_ID_009');
    const tmpTableName = utility.getTempTableName('Rating');
    const procedureQuery = getProcedureQuery(tmpTableName);
    const tx = cds.transaction();
    const entityKeys = JSON.stringify(utility.csnKeyFromKeyAndResult(Rating.keys, rating));

    try {
      const fillTempTableQuery = await utility.getTempTableQuery(
        tx,
        'Rating',
        rating,
        tmpTableName,
        true,
        JSON.stringify(entityKeys)
      );

      // const fillTempTableQuery = `CREATE LOCAL TEMPORARY COLUMN TABLE "${tmpTableName}" AS ( SELECT \nNULL AS "ApprovalDate",\nNULL AS "RatingDescription",\n'BP_ID_009' AS "ASSOC_BusinessPartner.BusinessPartnerID",\nNULL AS "Commissioned",\nNULL AS "RatingMethodVersion",\n'' AS "ASSOC_FinancialContract.FinancialContractID",\nNULL AS "CreationDate",\nNULL AS "RatingOutlook",\n'' AS "ASSOC_FinancialContract.IDSystem",\nNULL AS "Endorsed",\nNULL AS "RatingRank",\n'' AS "ASSOC_GeographicalRegion.GeographicalStruc…RatingCategory",\nNULL AS "ChangeTimestampInSourceSystem",\n'2234-02-28' AS "BusinessValidTo",\n'I' AS "Rating",\n'' AS "RatingMethod",\nNULL AS "ChangingUserInSourceSystem",\nNULL AS "ASSOC_EmployeeResponsibleForRating.BusinessPartnerID",\nNULL AS "RatingBasedOnMarginsOfConservatism",\n'Published' AS "RatingStatus",\nNULL AS "ChangingProcessType",\nNULL AS "_EmployeeApprovingTheRating.BusinessPartnerID",\nNULL AS "RatingComment",\n'ShortTerm' AS "TimeHorizon",\nNULL AS "ChangingProcessID"\n FROM "DUMMY"\n);`;
      await tx.run(fillTempTableQuery);
      const result = await tx.run(procedureQuery);
      return rating;
    } catch (ex) {
      console.log(ex);
    }
  });

  srv.on('removeNewRatings', async (req) => {});

  srv.on('removeAllClaims', async (req) => {
    try {
      const result = await INSERT.into(Test).entries({
        TestString: 'Test',
        TestBoolean: true,
        TestDate: '2021-01-01',
        TestDecimal: parseFloat('100,12')
      });
      console.log(result);
    } catch (ex) {
      console.log(ex);
    }
  });

  /**
   * Returns the query to call the correct validation procedure
   * @returns {string} - The query statement
   */
  function getProcedureQuery(tmpTableName) {
    return `CALL "RatingLoad_Procedure"("${tmpTableName}")`;
  }

  function getDummyRating(rating, bp) {
    const randomDate = getRandomDate();
    return {
      IsFxRating: false,
      RatingAgency: 'S&P',
      RatingCategory: 'ExternalRating',
      RatingMethod: '1',
      RatingStatus: 'Published',
      TimeHorizon: 'ShortTerm',
      ASSOC_BusinessPartner_BusinessPartnerID: bp,
      ASSOC_FinancialContract_FinancialContractID: '1',
      ASSOC_FinancialContract_IDSystem: '1',
      ASSOC_GeographicalRegion_GeographicalStructureID: '1',
      ASSOC_GeographicalRegion_GeographicalUnitID: '1',
      _Security_FinancialInstrumentID: '1',
      BusinessValidFrom: randomDate.businessValidFrom,
      BusinessValidTo: randomDate.businessValidTo,
      Rating: rating
    };
  }

  function getRandomDate() {
    const fromDay = Math.floor(Math.random() * (30 - 1 + 1)) + 1;
    const toDay = fromDay + 1;
    const monthTmp = Math.floor(Math.random() * (12 - 1 + 1)) + 1;
    const month = monthTmp < 10 ? `0${monthTmp}` : monthTmp;
    const year = Math.floor(Math.random() * (2999 - 2100 + 1)) + 2100;

    const businessValidFrom = `${year}-${month}-${fromDay}`;
    const businessValidTo = `${year}-${month}-${toDay}`;

    return { businessValidFrom, businessValidTo };
  }
};
