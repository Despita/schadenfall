/**
 * Helper-Module which provides convenience functions for executing sql statements
 * @module utility
 */
const path = require('path');
const _ = require('lodash/fp');
const { v4: uuidv4 } = require('uuid');
const { TextBundle } = require('@sap/textbundle');

/**
 * Get the right Textbundle based on the given locale
 *
 * @param {Object} locale - The locale
 * @returns
 */
exports.getTextBundle = function (locale) {
  const i18nPath = path.resolve(__dirname, '..', '_i18n/i18n');
  return new TextBundle(i18nPath, locale);
};

/**
 * creates a sql statement to create a temporarily used column table. This temporary table is then used via a stored procedure
 * to push approval data into the hdi container the business objects reside in.
 * @returns an sql statement that creates a temporary column table
 * @param tx db transaction to use
 * @param changedEntityName name of the entity that has been changed in the approval
 * @param changedEntityData data of the entity that has been chanted in the approval
 * @param changedEntityKey key of the entity ...
 * @param tmpTableName the name of the temporary table to use in the SQL statement
 * @param create flag that indicates if the data of the approval should be created or updated
 */
exports.getTempTableQuery = async (
  tx,
  changedEntityName,
  changedEntityData,
  tmpTableName,
  create = false,
  changedEntityKey = []
) => {
  const tableTypeResultSet = await getTableTypeResultSet(tx, changedEntityName);
  const rootEntityName = changedEntityName;
  const sqlArr = [],
    fieldsArr = [];
  let where = '';

  if (tableTypeResultSet.length > 0) {
    sqlArr.push('CREATE LOCAL TEMPORARY COLUMN TABLE "' + tmpTableName + '" AS ( SELECT ');

    for (let i = 0; i < tableTypeResultSet.length; i++) {
      const tableType = tableTypeResultSet[i];
      const key = tableType.COLUMNNAME;
      const keyWithoutDot = replaceDotsWithUnderscores(key);
      const changedEntityDataArr = _.toPairs(changedEntityData);
      const foundEntityData = _.find((obj) => obj[0] === keyWithoutDot || obj[0] === keyWithoutDot.toUpperCase())(
        changedEntityDataArr
      );
      // TODO: SQL-Injection !!!
      if (foundEntityData) {
        // Check if boolean value
        if (foundEntityData[1] === false || foundEntityData[1] === true) {
          fieldsArr.push(foundEntityData[1] + ' AS "' + key + '",');
        } else {
          fieldsArr.push("'" + foundEntityData[1] + '\' AS "' + key + '",');
        }
      } else {
        if (create) {
          fieldsArr.push('NULL AS "' + key + '",');
        } else {
          fieldsArr.push('"' + keyWithoutDot + '" AS "' + key + '",');
        }
      }
    }

    // replace trailing , from last fieldsArr element
    fieldsArr[fieldsArr.length - 1] = fieldsArr[fieldsArr.length - 1].replace(/,$/, '');
    // add fieldsArr elements to sqlArr elements
    sqlArr.push(...fieldsArr);
    if (create) {
      sqlArr.push(' FROM "DUMMY"');
    } else {
      const changedEntityKeyValue = JSON.parse(jsonKeyValueForChangedEntityKey(changedEntityKey));
      sqlArr.push(' FROM "' + rootEntityName + '_View"(CURRENT_DATE,CURRENT_TIMESTAMP) WHERE');

      for (var prop in changedEntityKeyValue) {
        if (where !== '') {
          where += ' AND';
        }
        where += '"' + prop + '" = \'' + changedEntityKeyValue[prop] + "'";
      }
      sqlArr.push(where);
    }
    sqlArr.push(');');
  }
  return sqlArr.join('\n');
};

exports.getTempTableName = (entityName) => {
  return '#' + entityName + uuidv4() + 'Tmp';
};

exports.csnKeyFromKeyAndResult = (keys, result) => {
  var changedEntityKeyObject = [];
  changedEntityKeyObject.push('(');
  for (let [key] of Object.entries(keys)) {
    changedEntityKeyObject.push({ ref: [key] });
    changedEntityKeyObject.push('=');
    changedEntityKeyObject.push({ val: result[key] });
    changedEntityKeyObject.push('and');
  }
  changedEntityKeyObject.splice(changedEntityKeyObject.length - 1, 1);
  changedEntityKeyObject.push(')');
  return changedEntityKeyObject;
};

async function getTableTypeResultSet(tx, entityName) {
  // Check if changedEntityName has a Parent
  const tableName = 'sap.fsdm.tabletypes::' + entityName + 'TT';
  var sqlReadTT = 'SELECT DISTINCT TABLENAME, COLUMNNAME FROM SF_TABLECOLUMN WHERE TABLENAME = ?;';
  var args = [tableName];
  var tableTypeResultSet = await tx.run(sqlReadTT, args);
  return tableTypeResultSet;
}

function replaceDotsWithUnderscores(input) {
  return input.replace(/\./g, '_');
}

function jsonKeyValueForChangedEntityKey(value) {
  if (!value) {
    return '';
  }
  var oResult = {};
  var key = '';
  var oValue = JSON.parse(value);
  for (var prop in oValue) {
    if (typeof oValue[prop].ref !== 'undefined') {
      key = oValue[prop].ref[0];
    }
    if (typeof oValue[prop].val !== 'undefined') {
      oResult[key] = oValue[prop].val;
    }
  }
  return JSON.stringify(oResult);
}
