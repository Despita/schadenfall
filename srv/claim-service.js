const cds = require('@sap/cds');
const LOG = cds.log('[Schadenfall - ClaimService]', cds.log.levels.TRACE);
const utility = require('./util/utility');
const { Claim } = cds.entities('');

module.exports = (srv) => {
  srv.on('checkRatings', async (req) => {
    const tx = cds.transaction(req);
    let query = getRatingQuery();
    let ratings;

    //1. Read Ratings
    try {
      ratings = await tx.run(query);

      for (let i = 0; i < ratings.length; i++) {
        let rating = ratings[i];
        switch (rating.RATING) {
          case 'C':
            await handle_C_Rating(rating, tx);
            break;
          case 'I':
            await handle_I_Rating(rating, tx);
            break;
          default:
            await handleGoodRating(rating, tx);
            break;
        }
      }
    } catch (oEx) {
      LOG.debug(oEx.message);
    }
  });
};

/**
 * Handles the rating based on rating C
 * @param {Object} rating - The new rating
 * @param {Object} tx - A transaction object
 */
async function handle_C_Rating(rating, tx) {
  const claims = await checkExisitingClaims(rating, tx);

  //Create new claims if no claims where found for this BusinessPartner/LastRatingDate
  if (claims.length === 0) {
    const loans = await getLoans(tx, rating.ASSOC_BUSINESSPARTNER_BUSINESSPARTNERID);
    return createNewClaims(loans, rating, tx);
  }
  LOG.debug(
    `There are no new or existing claims for Businesspartner: ${rating.ASSOC_BUSINESSPARTNER_BUSINESSPARTNERID} to be inserted or updated`
  );
}

/**
 * Handles the rating based on rating I
 * @param {Object} rating - The new rating
 * @param {Object} tx - A transaction object
 */
async function handle_I_Rating(rating, tx) {
  const claims = await checkExisitingClaims(rating, tx);

  //Create new claims if no claims where found for this BusinessPartner/LastRatingDate
  if (claims.length === 0) {
    const loans = await getLoans(tx, rating.ASSOC_BUSINESSPARTNER_BUSINESSPARTNERID);
    return createNewClaims(loans, rating, tx);
  }

  //Update claims if there are already claims for this Businesspartner
  return updateClaims(claims, rating, tx);
}

/**
 * Handles the rating based on a good rating like B or A
 * @param {Object} rating - The new rating
 * @param {Object} tx - A transaction object
 */
async function handleGoodRating(rating, tx) {}

async function checkExisitingClaims(rating, tx) {
  LOG.debug(`Reading existing Claims for BusinessPartnerID: ${rating.ASSOC_BUSINESSPARTNER_BUSINESSPARTNERID}`);
  return tx.run(
    SELECT.from(Claim).where({
      BUSINESSPARTNERID: rating.ASSOC_BUSINESSPARTNER_BUSINESSPARTNERID,
      LASTRATINGDATE: rating.BUSINESSVALIDTO,
      RATING: rating.RATING
    })
  );
}

/**
 * Creates new entries in the claim table
 * @param {Object} loans - The loans of the businesspartner
 * @param {Object} rating - The new rating
 * @param {Object} tx - A transaction object
 */
async function createNewClaims(loans, rating, tx) {
  const claims = [];

  //1 Create claims per loan of the BusinessPartner
  for (let loan of loans) {
    claims.push({
      BusinessPartnerID: loan.BUSINESSPARTNERID,
      LoanID: loan.FINANCIALCONTRACTID,
      IDSystem: loan.IDSYSTEM,
      BusinessValidFrom: loan.BUSINESSVALIDFROM,
      BusinessValidTo: loan.BUSINESSVALIDTO,
      Amount: loan.NOMINALAMOUNT,
      LastRatingDate: rating.BUSINESSVALIDTO,
      Status_code: 'O',
      Processed: false,
      Currency: loan.NOMINALAMOUNTCURRENCY,
      Rating: rating.RATING
    });
  }

  if (claims.length > 0) {
    LOG.debug(
      `Inserting ${loans.length} new claims for Businesspartner: ${rating.ASSOC_BUSINESSPARTNER_BUSINESSPARTNERID}`
    );
    return tx.run(INSERT.into(Claim).entries(claims));
  }
  LOG.debug(
    `There are no new claims for Businesspartner: ${rating.ASSOC_BUSINESSPARTNER_BUSINESSPARTNERID} to be inserted`
  );
}

/**
 *
 * @param {Object[]} claims - The claims, which sould be updated
 * @param {Object} rating - The rating object
 * @param {Object} tx - The transaction object
 */
async function updateClaims(claims, rating, tx) {
  //Check if there has been new loans since last insert of claims
  const loans = await getLoans(tx, rating.ASSOC_BUSINESSPARTNER_BUSINESSPARTNERID);
  const claimPromises = [];
  const newLoans = loans.filter((loan) => {
    return !claims.some((claim) => {
      return (
        claim.LoanID === loan.FINANCIALCONTRACTID &&
        claim.IDSystem === loan.IDSYSTEM &&
        claim.BusinessValidFrom === loan.BUSINESSVALIDFROM &&
        claim.BusinessValidTo === loan.BUSINESSVALIDTO
      );
    });
  });

  //Create new claims
  await createNewClaims(newLoans, rating, tx);

  //Update existing claims (Only Rating)
  for (let claim of claims) {
    if (claim.Rating !== rating.RATING) {
      claim.Rating = rating.RATING;
      claimPromises.push(
        UPDATE.entity(Claim).where({
          ID: claim.ID
        })
      );
    }
  }

  if (claimPromises.length > 0) {
    LOG.debug(
      `Updating ${claimPromises.length} existing claims for Businesspartner: ${rating.ASSOC_BUSINESSPARTNER_BUSINESSPARTNERID}`
    );
    return Promise.all(claimPromises);
  }
  LOG.debug(`All existing claims Businesspartner: ${rating.ASSOC_BUSINESSPARTNER_BUSINESSPARTNERID} are up to date`);
}

/**
 *
 * @param {String} bp - The businesspartner ID
 * @returns
 */
function getLoanQuery(bp) {
  return cds.parse.cql(`SELECT FROM BUSINESSPARTNERTOFINANCIALCONTRACT WHERE BUSINESSPARTNERID = '${bp}'`);
}

/**
 * Return the cql query object to get the newest ratings
 * @returns
 */
function getRatingQuery() {
  return cds.parse.cql(`SELECT FROM RATING WHERE BUSINESSVALIDTO = (SELECT MAX(BUSINESSVALIDTO) FROM RATING)`);
}

/**
 *
 * @param {Object} tx - A transaction object
 * @param {String} query - The query string
 * @param {String} bp - The businesspartner ID
 * @returns
 */
function getLoans(tx, bp) {
  LOG.debug(`Reading Loans for BusinessPartnerID: ${bp}`);
  const query = getLoanQuery(bp);
  return tx.run(query);
}
