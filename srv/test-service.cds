using sf as _Test from '../db/test-data-model';
using sf as _Rating from '../db/Rating-data-model';

service TestService @(
    impl     : './test-service.js',
    path     : 'Test',
    requires : 'authenticated-user'
) {

    entity Test @(
        Capabilities : {
            InsertRestrictions : {Insertable : true},
            UpdateRestrictions : {Updatable : true},
            DeleteRestrictions : {Deletable : true}
        },
        restrict     : [{
            grant : ['CREATE'],
            to    : 'admin'
        }, ]
    )                     as projection on _Test.Test;

    action testAction();
    action createNewRatings();
    action removeNewRatings();
    action removeAllClaims();

}
