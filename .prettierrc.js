// see https://www.robertcooper.me/using-eslint-and-prettier-in-a-typescript-project for config details
module.exports = {
  semi: true,
  trailingComma: "none",
  singleQuote: true,
  printWidth: 120,
  useTabs: false,
  tabWidth: 2,
};
